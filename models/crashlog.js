"use strict";

module.exports = function(sequelize, DataTypes) {
    var Crashlog = sequelize.define("Crashlog", {
        UUID: DataTypes.STRING,
        Logtxt: DataTypes.TEXT
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    return Crashlog;
};
