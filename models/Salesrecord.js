"use strict";

module.exports = function(sequelize, DataTypes) {
    var Salesrecord = sequelize.define("Salesrecord", {
        Salesname: DataTypes.STRING,
        Phone: DataTypes.STRING,
        Cmsdate: DataTypes.DATE,
        Remark: DataTypes.STRING
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Salesrecord.associate = function(models) {
        Salesrecord.belongsTo(models.Sales, {
            foreignKey: {
                allowNull: true
            }
        });
        Salesrecord.belongsTo(models.Member, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Salesrecord;
};
