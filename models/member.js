"use strict";

module.exports = function(sequelize, DataTypes) {
    var Member = sequelize.define("Member", {
        Account:  DataTypes.STRING,
        Phone: DataTypes.STRING,
        Sum1: DataTypes.INTEGER,  //上一個月消費記錄
        Sum2: DataTypes.INTEGER,  //總消費次
        Remark: DataTypes.STRING,
        Deleteflag:DataTypes.STRING,
        State: DataTypes.STRING //Y:一般 N:黑名單
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Member.associate = function(models) {
        Member.hasMany(models.Salesrecord);
        Member.hasMany(models.Phonerecord);
    }
    return Member;
};
