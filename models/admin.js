/**
 * Created by ralph on 7/24/2017.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
    var Admin = sequelize.define("Admin", {
        Account: {type:DataTypes.STRING,unique:true},
        Password: DataTypes.STRING,
        Token: DataTypes.STRING,
        Type: DataTypes.STRING(1) //0:最高權限 1:不可增加管理者
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    return Admin;
};