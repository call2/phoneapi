"use strict";

module.exports = function(sequelize, DataTypes) {
    var Phonerecord = sequelize.define("Phonerecord", {
        Salesname: DataTypes.STRING,
        Phone: DataTypes.STRING,
        Calldate: DataTypes.DATE,
        Version:DataTypes.STRING,
        Inout:DataTypes.STRING
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Phonerecord.associate = function(models) {
        Phonerecord.belongsTo(models.Sales, {
            foreignKey: {
                allowNull: true
            }
        });
        Phonerecord.belongsTo(models.Member, {
            foreignKey: {
                allowNull: true
            }
        });
    }

    return Phonerecord;
};
