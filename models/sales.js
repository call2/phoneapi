"use strict";

module.exports = function(sequelize, DataTypes) {
    var Sales = sequelize.define("Sales", {
        UUID: DataTypes.STRING,
        Salesname: DataTypes.STRING,
        Zip: DataTypes.STRING,    //郵遞區號
        Show1: DataTypes.STRING,  //是否顯示消費紀錄
        Show2: DataTypes.STRING,  //是否顯示黑名單
        Show3: DataTypes.STRING, //是否顯示會員備註
        Show4: DataTypes.STRING,//是否顯示回報
        State: DataTypes.STRING  //Y:啟用  N:刪除
    },{charset: 'utf8',collate: 'utf8_unicode_ci'});

    Sales.associate = function(models) {
        Sales.hasMany(models.Salesrecord);
        Sales.hasMany(models.Phonerecord);
    }
    return Sales;
};
