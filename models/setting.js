module.exports = function(sequelize, DataTypes) {
  var Setting = sequelize.define("Setting", {
    Starttime: DataTypes.STRING, //HH:mm
    Endtime: DataTypes.STRING ,   //HH:mm
    Cnt:DataTypes.INTEGER
  },{charset: 'utf8',collate: 'utf8_unicode_ci'});
  return Setting;
};