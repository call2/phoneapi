/**
 * Created by ralph on 7/25/2017.
 */
var models  = require('../../models');
var express = require('express');
var router = express.Router();

//Errors
var errors = require('../../lib/errors');
//box logger
var boxlogger = require('../../lib/boxloger');
// config
var config = require('config.json')('setting.json');
// moment: date/time format
var moment = require('moment-timezone');
// winston logging
var winston = require('winston');
// password hash
var CryptoJS = require("crypto-js");
var SHA256 = require('crypto-js/sha256');
// generate access token
var randtoken = require('rand-token');
var tool = require('../../lib/tool');
// var Q = require('q');

/**
 * @api {post} /adminmgr/loginAdmin  0101.loginAdmin
 * @apiName loginAdmin
 * @apiGroup Admin
 *
 * @apiDescription  管理員登入
 *
 * @apiParam {String} Password 密碼
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {String} Token  Token
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 * @apiError 401 密碼錯誤
 *
 * @apiSampleRequest /adminmgr/loginAdmin
 */

router.post('/loginAdmin', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/loginAdmin request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/loginAdmin response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/loginAdmin response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    var hash = SHA256(req.body.Password);
    var pswd=hash.toString(CryptoJS.enc.Base64);
    models.Admin.findOne({where:{Account:"admin1"}})  //todo 測試階段  admin1 改為 admin2
        .then(function (user) {
            if(user != null){
                //檢查密碼
                if(user.Password == pswd){  //
                    //如有一致
                    var newToken = randtoken.uid(config.TokenLength);
                    user.Token = newToken;

                    user.save(function (err) {
                    }).then(function () {
                        response['Token'] = user.Token;
                        finish(null, response, "0","");
                    }).catch(function (err0) {
                        finish(null, response, "999",err0.message);
                    });

                } else {
                    //如不一致, 密碼錯誤
                    finish(null, response, "112","");
                }

            } else {
                //查無此帳號
                finish(null, response, "103","");
            }

        }).catch(function (err) {
        finish(null, response, "999",err.message);
    });
});

/**
 * @api {post} /adminmgr/getSaleslist  0102.getSaleslist
 * @apiName getSaleslist
 * @apiGroup Admin
 *
 * @apiDescription  業務人員管理/ 查詢業務人員列表
 *
 * @apiParam {String} Token  Token
 * @apiParam {String} Qrystr  查詢關鍵字
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Sales} Sales  業務人員清單
 * @apiSuccess {String} Sales.Salesid  業務人員ID
 * @apiSuccess {String} Sales.UUID  業務人員UUID
 * @apiSuccess {String} Sales.Salesname  業務人員名稱
 * @apiSuccess {String} Sales.Show1  是否顯示消費紀錄
 * @apiSuccess {String} Sales.Show2  是否顯示黑名單
 * @apiSuccess {String} Sales.Show3  是否顯示會員備註
 * @apiSuccess {String} Sales.Show4  是否顯示回報
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查此資料
 *
 * @apiSampleRequest /adminmgr/getSaleslist
 */

router.post('/getSaleslist', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getSaleslist request: ' + JSON.stringify(req.body));

    var response = {};
    response["Sales"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getSaleslist response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getSaleslist response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user==null){
                finish(1, response, "100","");
            }
            else {
                var arr=[{State:"Y"}];
                if (req.body.Qrystr=="") arr.push({$or:[{Salesname:{$like:"%"+req.body.Qrystr+"%"}},{UUID:{$like:"%"+req.body.Qrystr+"%"}}]});
                var qryparm={where:{$and:arr}};
                models.Sales.count(qryparm).then(function (cnt) {
                    response["Cnt"]=cnt;
                    models.Sales.findAll(qryparm).then(function (docs) {
                        if (docs==null || docs.length==0){
                            finish(1, response, "103","");
                        }
                        else {
                            for  (var i=0;i<docs.length;++i){
                                var sales={Salesid:docs[i].id,UUID:docs[i].UUID,Salesname:docs[i].Salesname,Show1:docs[i].Show1,Show2:docs[i].Show2,Show3:docs[i].Show3,Show4:docs[i].Show4,Zip:docs[i].Zip};
                                response["Sales"].push(sales);
                            }
                            finish(0, response, "0","");
                        }
                    })
                })
            }
        })
    }
});

/**
 * @api {post} /adminmgr/setSalesinfo  0103.setSalesinfo
 * @apiName setSalesinfo
 * @apiGroup Admin
 *
 * @apiDescription  業務人員管理 / 業務人員權限設定
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Salesid  業務人員ID
 * @apiParam {String} Salesname  業務人員名稱
 * @apiParam {String} Show1  是否顯示消費紀錄
 * @apiParam {String} Show2  是否顯示黑名單
 * @apiParam {String} Show3  是否顯示會員備註
 * @apiParam {String} Show4  是否顯示回報
 * @apiParam {String} Zip  郵遞區號
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 * @apiError 401 密碼錯誤
 *
 * @apiSampleRequest /adminmgr/setSalesinfo
 */

router.post('/setSalesinfo', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/setSalesinfo request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/setSalesinfo response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/setSalesinfo response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (admin) {
            if (admin!=null){
                models.Sales.findOne({where:{id:req.body.Salesid,State:"Y"}}).then(function (user) {
                    if (user!=null){
                        user["Salesname"]=req.body.Salesname;
                        user["Show1"]=req.body.Show1;
                        user["Show2"]=req.body.Show2;
                        user["Show3"]=req.body.Show3;
                        user["Show4"]=req.body.Show4;
                        user["Zip"]=req.body.Zip;
                        user.save().then(function () {
                            finish(0, response, "0","");
                        })
                    }
                    else
                        finish(1, response, "103","");
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
});

/**
 * @api {post} /adminmgr/deleteSales  010305.deleteSales
 * @apiName deleteSales
 * @apiGroup Admin
 *
 * @apiDescription  業務人員管理 / 刪除業務人員
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Salesid  業務人員ID
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 * @apiError 401 密碼錯誤
 *
 * @apiSampleRequest /adminmgr/deleteSales
 */

router.post('/deleteSales', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/deleteSales request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/deleteSales response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/deleteSales response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (admin) {
            if (admin!=null){
                models.Sales.findOne({where:{id:req.body.Salesid,State:"Y"}}).then(function (user) {
                    if (user!=null){
                        user["State"]="N";
                        user.save().then(function () {
                            finish(0, response, "0","");
                        })
                    }
                    else
                        finish(null, response, "103","");
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
});

/**
 * @api {post} /adminmgr/getMemberlist  0104.getMemberlist
 * @apiName getMemberlist
 * @apiGroup Admin
 *
 * @apiDescription  會員管理/ 查詢會員列表
 *
 * @apiParam {String} Token  Token
 * @apiParam {String} State  查詢身分 "":全部  Y:一般會員  N:黑名單
 * @apiParam {String} Qrystr  查詢電話
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Memberlist} Memberlist  會員清單
 * @apiSuccess {String} Memberlist.Memberid  會員ID
 * @apiSuccess {String} Memberlist.Sum1  上一個月消費次數
 * @apiSuccess {String} Memberlist.Sum2  總消費次數
 * @apiSuccess {String} Memberlist.State  身份狀態
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查此資料
 *
 * @apiSampleRequest /adminmgr/getMemberlist
 */

router.post('/getMemberlist', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getMemberlist request: ' + JSON.stringify(req.body));

    var response = {};
    response["Memberlist"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getMemberlist response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getMemberlist response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""||req.body.State==null||req.body.Qrystr==null){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user==null){
                finish(1, response, "100","");
            }
            else {
                var arr=[{Deleteflag:"N"}]
                if (req.body.Qrystr!=""){
                  arr.push({$or:[{Phone:{$like:"%"+req.body.Qrystr+"%"}},{Remark:{$like:"%"+req.body.Qrystr+"%"}}]});
                }

                if (req.body.State!="")
                    arr.push({State:req.body.State});
                var qryparm={where:{$and:arr},include:[{model:models.Salesrecord}]};

                var chkdate1=getPrevMonth(1);
                // console.log("*******getPrevMonth******")
                models.Member.count(qryparm).then(function (cnt) {
                    response["Cnt"]=cnt;
                    // qryparm["offset"]=50*req.body.Pageno;
                    // qryparm["limit"]=50;
                    models.Member.findAll(qryparm).then(function (docs) {
                        if (docs==null || docs.length==0){
                            finish(1, response, "103","");
                        }
                        else {
                            for  (var i=0;i<docs.length;++i){
                                var remark="";
                                if (docs[i].Salesrecords!=null && docs[i].Salesrecords.length>0){
                                    var tmparr=[];
                                    for (var j=0;j<docs[i].Salesrecords.length;++j)
                                        tmparr.push(docs[i].Salesrecords[j].dataValues)
                                  //檢查前三個月及一個月消費紀錄 若不同則update
                                  var pcnt3=0,pcnt1=0;
                                  // var prev3=tmparr.filter(function (val) {
                                  //   return val.Cmsdate>=chkdate3[1] && val.Cmsdate<chkdate3[0];
                                  // })
                                  pcnt3=docs[i].Salesrecords.length;
                                  var prev1=docs[i].Salesrecords.filter(function (val) {
                                    return val.Cmsdate>=chkdate1[1] && val.Cmsdate<chkdate1[0];
                                  })
                                  pcnt1=prev1.length;
                                  // if (pcnt3>0){
                                  //     var prev1=docs[i].Salesrecords.filter(function (val) {
                                  //       return val.Cmsdate>=chkdate1[1] && val.Cmsdate<chkdate1[0];
                                  //     })
                                  //   pcnt1=prev1.length;
                                  // }
                                  if (parseInt(pcnt1)!=parseInt(docs[i].Sum1) || parseInt(pcnt3)!=parseInt(docs[i].Sum2)){
                                    // console.log("***mbr update*:"+ docs[i].id+ "  ***pcnt1*: "+pcnt1+"  pcnt3:"+pcnt3+ " sum1:"+docs[i].Sum1 +" sum2:"+docs[i].Sum2);
                                    docs[i].Sum2=pcnt3;docs[i].Sum1=pcnt1;
                                    docs[i].save();
                                  }
                                  //
                                    var tmp=tmparr.filter((function (ele) {
                                        return ele.Remark!="" && ele.Remark!=null;
                                    }))
                                    tmp.sort(function (a, b) {
                                        return (a.Cmsdate < b.Cmsdate) ? 1 : -1;
                                    })
                                    remark=(tmp[0]!=undefined)?tmp[0].Remark:"";
                                }
                                var mbr={Memberid:docs[i].id,Phone:docs[i].Phone,Sum1:docs[i].Sum1,Sum2:docs[i].Sum2,Remark:remark,Mremark:docs[i].Remark,State:docs[i].State};
                                response["Memberlist"].push(mbr);
                            }
                            finish(0, response, "0","");
                        }
                    })
                })
            }
        })
    }
});

/**
 * @api {post} /adminmgr/setMbrstate  010405.setMbrstate
 * @apiName setMbrstate
 * @apiGroup Admin
 *
 * @apiDescription  會員管理/  設定會員黑名單(恢復)
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid  會員ID
 * @apiParam {String} State  Y:一般會員 N:黑名單
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/setMbrstate
 */

router.post('/setMbrstate', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/setMbrstate request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/setMbrstate response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/setMbrstate response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                models.Member.findOne({where:{id:req.body.Memberid,Deleteflag:"N"}}).then(function (doc) {
                    if (doc==null){
                        finish(1, response, "103","");
                    }
                    else {
                        doc.State=req.body.State;
                        doc.save().then(function () {
                            finish(0, response, "0","");
                        })
                    }
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }

});

/**
 * @api {post} /adminmgr/deleteMember  010408.deleteMember
 * @apiName deleteMember
 * @apiGroup Admin
 *
 * @apiDescription  會員管理/ 刪除會員
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid  會員ID
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/deleteMember
 */

router.post('/deleteMember', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/deleteMember request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/deleteMember response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/deleteMember response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                models.Member.findOne({where:{id:req.body.Memberid,Deleteflag:"N"}}).then(function (doc) {
                    if (doc==null){
                        finish(1, response, "103","");
                    }
                    else {
                        doc.Deleteflag="Y";
                        doc.save().then(function () {
                            finish(0, response, "0","");
                        })
                    }
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }

});

/**
 * @api {post} /adminmgr/setMbrremark  0105.setMbrremark
 * @apiName setMbrremark
 * @apiGroup Admin
 *
 * @apiDescription  會員管理/  編輯會員備註
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid  會員ID
 * @apiParam {String} Remark  備註
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/setMbrremark
 */

router.post('/setMbrremark', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/setMbrremark request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/setMbrremark response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/setMbrremark response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                models.Member.findOne({where:{id:req.body.Memberid,Deleteflag:"N"}}).then(function (doc) {
                    if (doc==null){
                        finish(1, response, "103","");
                    }
                    else {
                        doc.Remark=req.body.Remark;
                        doc.save().then(function () {
                            finish(0, response, "0","");
                        })
                    }
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }

});

/**
 * @api {post} /adminmgr/getMbrrecord  0106.getMbrrecord
 * @apiName getMbrrecord
 * @apiGroup Admin
 *
 * @apiDescription  會員管理/  會員消費紀錄查詢
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid   會員ID
 * @apiParam {String} Startym  起始年月(YYYY/MM)
 * @apiParam {String} Endym   結束年月(YYYY/MM)
 * @apiParam {String} Salesname   業務員
 * @apiParam {String} Qrystr   查詢備註關鍵字
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {String} Mbrremark  會員備註
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Records} Records  消費紀錄清單
 * @apiSuccess {String} Records.Datestr  年月日
 * @apiSuccess {String} Records.Salesname  業務員
 * @apiSuccess {String} Records.Remark  消費備註
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getMbrrecord
 */

router.post('/getMbrrecord', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getMbrrecord request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getMbrrecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getMbrrecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""||req.body.Memberid==null||req.body.Startym==null||req.body.Endym==null||req.body.Salesname==null||req.body.Qrystr==null){
        finish(1, response, "250","");
    }
    else if (req.body.Memberid==0){
        finish(1, response, "203","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                var arr=[{MemberId:req.body.Memberid}];
                if (req.body.Qrystr!="")
                    arr.push({Remark:{$like:"%"+req.body.Qrystr+"%"}});
                if (req.body.Salesname!=0)
                    arr.push({Salesname:{$like:"%"+req.body.Salesname+"%"}})
                if (req.body.Startym!=""){
                    var startdate=moment(req.body.Startym+"/01","YYYY/MM/DD");
                    arr.push({Cmsdate:{$gte:startdate}})
                }
                if (req.body.Endym!=""){
                    var enddate=moment(req.body.Endym+"/01","YYYY/MM/DD").add(1,"Months");
                    arr.push({Cmsdate:{$lt:enddate}})
                }
                var qryparm={where:{$and:arr}};
                models.Salesrecord.count(qryparm).then(function (cnt) {
                    response["Cnt"]=cnt;
                    // qryparm["offset"]=50*req.body.Pageno;
                    // qryparm["limit"]=50;
                    qryparm["include"]=[{model:models.Sales},{model:models.Member}];
                    models.Salesrecord.findAll(qryparm).then(function (docs) {
                        if (docs==null || docs.length==0){
                            finish(1, response, "103","");
                        }
                        else {
                            for  (var i=0;i<docs.length;++i){
                                if (i==0){
                                    response["Mbrremark"]=docs[i].Member.Remark;
                                }
                                var salesname=(docs[i].Sale!=null)?docs[i].Sale.Salesname:docs[i].Salesname;
                                var rec={Datestr:moment(docs[i].Cmsdate).tz("Asia/Taipei").format("YYYY.MM.DD"),Salesname:salesname,Remark:docs[i].Remark};
                                response["Records"].push(rec);
                            }
                            finish(0, response, "0","");
                        }
                    })
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
});

/**
 * @api {post} /adminmgr/getSalesrecord  0107.getSalesrecord
 * @apiName getSalesrecord
 * @apiGroup Admin
 *
 * @apiDescription  消費紀錄管理/  消費紀錄查詢
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid   會員ID
 * @apiParam {String} Startym  起始年月(YYYY/MM)
 * @apiParam {String} Endym   結束年月(YYYY/MM)
 * @apiParam {String} Salename   業務員
 * @apiParam {String} Qrystr   查詢備註關鍵字
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Records} Records  消費紀錄清單
 * @apiSuccess {Int} Records.id  消費紀錄ID
 * @apiSuccess {String} Records.Datestr  年月日
 * @apiSuccess {String} Records.Phone  會員電話
 * @apiSuccess {String} Records.Salesname  業務員
 * @apiSuccess {String} Records.Remark  消費備註
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getSalesrecord
 */

router.post('/getSalesrecord', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getSalesrecord request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getSalesrecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getSalesrecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""||req.body.Memberid==null||req.body.Startym==null||req.body.Endym==null||req.body.Salename==null||req.body.Qrystr==null){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                var arr=[];
                if (req.body.Qrystr!="")
                    arr.push({Phone:{$like:"%"+req.body.Qrystr+"%"}});
                if (req.body.Startym!=""){
                    var startdate=moment(req.body.Startym,"YYYY/MM/DD");//.tz("Asia/Taipei");
                    arr.push({Cmsdate:{$gte:startdate}})
                }
                else if (req.body.Salename=="" && req.body.Qrystr=="") {
                    arr.push({Cmsdate:{$gte:moment().tz("Asia/Taipei").add(-2,"Months")}});
                }
                if (req.body.Endym!=""){
                    var enddate=moment(req.body.Endym,"YYYY/MM/DD").add(1,"Days");
                    arr.push({Cmsdate:{$lt:enddate}})
                }
                else if (req.body.Salename=="" && req.body.Qrystr=="") {
                    arr.push({Cmsdate:{$lt:moment().tz("Asia/Taipei").add(1,"Minutes")}})
                }
                if (req.body.Salename!=""){
                    arr.push({Salesname:{$like:'%'+req.body.Salename+'%'}});
                }
                var qryparm={where:{$and:arr}};
                // console.log(JSON.stringify(qryparm));
                models.Salesrecord.count(qryparm).then(function (cnt) {
                    response["Cnt"]=cnt;
                    // qryparm["offset"]=50*req.body.Pageno;
                    // qryparm["limit"]=50;
                    qryparm["include"]=[{model:models.Sales}];
                    qryparm["order"]=[["Cmsdate","DESC"]];

                    models.Salesrecord.findAll(qryparm).then(function (docs) {
                        if (docs==null || docs.length==0){
                            finish(1, response, "103","");
                        }
                        else {
                            for  (var i=0;i<docs.length;++i){
                                var salesname=(docs[i].Sale!=null)?docs[i].Sale.Salesname:docs[i].Salesname;
                                var rec={id:docs[i].id, Datestr:moment(docs[i].Cmsdate).tz("Asia/Taipei").format("YYYY.MM.DD"),Phone:docs[i].Phone,Salesname:salesname,Remark:docs[i].Remark};
                                response["Records"].push(rec);
                            }
                            finish(0, response, "0","");
                        }
                    })
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
});

/**
 * @api {post} /adminmgr/deleteSalesrecord  010705.deleteSalesrecord
 * @apiName deleteSalesrecord
 * @apiGroup Admin
 *
 * @apiDescription  消費紀錄管理/  消費紀錄刪除
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int[]} Arr   ID陣列
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/deleteSalesrecord
 */

router.post('/deleteSalesrecord', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/deleteSalesrecord request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/deleteSalesrecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/deleteSalesrecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""||req.body.Arr==null){
        finish(1, response, "250","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                models.Salesrecord.findAll({where:{id:req.body.Arr},include:[{model:models.Member}]}).then(function (docs) {
                    if (docs!=null){
                        cb0cnt=docs.length;
                        for (var i=0;i<docs.length;++i){
                            if (docs[i].Member!=null){
                                var qry="update Members set Sum2=Sum2-1 where id="+docs[i].Member.id;
                                if (chkPrevMonth(docs[i].Cmsdate)) qry="update Members set Sum1=Sum1-1, Sum2=Sum2-1 where id="+docs[i].Member.id;
                                tool.query(qry,function () {
                                    chkComplete();
                                })
                            }
                        }
                    }
                    else {
                        finish(1, response, "103","");
                    }
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
    var cb0cnt=0;
    function chkComplete() {
        cb0cnt--;
        if (cb0cnt==0){
            var ids="("+ req.body.Arr.join(",")+")";
            var sql="delete from Salesrecords where id in "+ids;
            tool.query(sql,function () {
                finish(0, response, "0","");
            })
        }
    }
});

/**
 * @api {post} /adminmgr/importSalesrecord  0108.importSalesrecord
 * @apiName importSalesrecord
 * @apiGroup Admin
 *
 * @apiDescription  消費紀錄管理/  匯入消費紀錄
 *
 * @apiParam {String} Token  Token
 * @apiParam {JSON} Data[]   匯入資料JSON陣列 ex.:[{"YYYYMMDD":"2017/11/01","Salesname":"test1","Phone":"0911123123","Remark":"22212"},{"YYYYMMDD":"2017/11/02","Salesname":"test1","Phone":"0911123123","Remark":"22212"},...]
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/importSalesrecord
 */

router.post('/importSalesrecord', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/importSalesrecord request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/importSalesrecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/importSalesrecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };


    if (req.body.Token==null||req.body.Token==""||req.body.Data==null){
        finish(1, response, "250","");
    }
    else if (req.body.Data.length==0){
        finish(1, response, "204","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user==null){
                finish(null, response, "100","");
            }
            else {
                var obj=req.body.Data;
                var arr=[];
                cb1cnt=obj.length;
                //先檢查是否有新會員
                chkNewmember(obj,function (ok) {
                    obj.forEach(function (tmp) {
                        var phone=(tmp["Phone"].length==9)?"0"+tmp["Phone"]:tmp["Phone"];
                        getData(tmp["YYYYMMDD"],phone,tmp["Salesname"],tmp["Remark"],function (res) {
                            arr.push(res);
                            chkComplete1(arr);
                        })
                    })
                })
            }
        });
        var cb1cnt=0;
        function chkComplete1(arr) {
            --cb1cnt;
            if (cb1cnt==0){
                models.Salesrecord.bulkCreate(arr).then(function () {
                    finish(0, response, "0","");
                })
            }
        }
    }

    function getData(yyyymmdd, phone,name,remark,callback) {
        models.Sales.findOne({where:{Salesname:name}}).then(function (rec) {
            var salesid=null;
            if (rec!=null){
                salesid=rec.id;
            }
            models.Member.findOne({where:{Deleteflag:"N",Phone:phone}}).then(function (doc) {
                callback({Cmsdate:yyyymmdd,Salesname:name,Phone:phone,Remark:remark,SaleId:salesid,MemberId:doc.id});
            })
        })
    }
});

function chkPrevMonth(yyyymmdd) {
    var inpdate=moment(yyyymmdd,"YYYY/MM/DD");
    var inpym=inpdate.format("YYYY/MM");
    var chkym=moment().add(-1,"Months").format("YYYY/MM");
    return (inpym==chkym);
}
function getPrevMonth(month) {
  var inpdate=moment().format("YYYY/MM")+"/01";
  var outdate1=moment(inpdate,"YYYY/MM/DD");
  var outdate2=moment(inpdate,"YYYY/MM/DD").add(-month,"Months");
  return [outdate1,outdate2];
}
function chkNewmember(obj,callback) {
    var arr=[],mbr=[],cb0=obj.length;
    obj.forEach(function (tmp) {
        var phone=(tmp["Phone"].length==9)?"0"+tmp["Phone"]:tmp["Phone"];
        models.Member.findOne({where:{Deleteflag:"N",Phone:phone}}).then(function (doc) {
           updateMbr(doc,tmp,function (res) {
               chkComplete0(res,tmp)
           })
        })
    })

    function updateMbr(doc,tmp,callback) {
        var phone=(tmp["Phone"].length==9)?"0"+tmp["Phone"]:tmp["Phone"];
        if (doc==null ){
            if (arr.indexOf(phone)<0){
                arr.push(phone);
                callback(true)
            }
            else {
                callback(false)
            }
        }
        else{
            var qry="update Members set Sum2=Sum2+1 where id="+doc.id;
            if (chkPrevMonth(tmp["YYYYMMDD"])) qry="update Members set Sum1=Sum1+1, Sum2=Sum2+1 where id="+doc.id;
            tool.query(qry,function () {
                callback(false)
            })
        }
    }
    function chkComplete0(add,tmp) {
        var phone=(tmp["Phone"].length==9)?"0"+tmp["Phone"]:tmp["Phone"];
        if (add) mbr.push({Phone:phone,Sum1:chkPrevMonth(tmp["YYYYMMDD"])?1:0,Sum2:1,State:"Y"});
        else {
            for (var i=0;i<mbr.length;++i){
                if (mbr[i]["Phone"]==phone){
                    mbr[i].Sum2+=1;
                    if (chkPrevMonth(tmp["YYYYMMDD"])) mbr[i].Sum1+=1;
                    break;
                }
            }
        }
        cb0--;
        if (cb0==0){
            models.Member.bulkCreate(mbr).then(function () {
                callback(true);
            })
        }
    }
}

/**
 * @api {post} /adminmgr/getPhonerecord  0109.getPhonerecord
 * @apiName getPhonerecord
 * @apiGroup Admin
 *
 * @apiDescription  來電記錄/  來電記錄查詢
 *
 * @apiParam {String} Token  Token
 * @apiParam {Int} Memberid   會員ID
 * @apiParam {String} Startym  起始年月(YYYY/MM)
 * @apiParam {String} Ismember  身份 "":全部 Y:會員 N:非會員
 * @apiParam {Int} Salesid   業務員ID
 * @apiParam {String} Qrystr   查詢電話號碼
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Records} Records  消費紀錄清單
 * @apiSuccess {String} Records.Datestr  年月
 * @apiSuccess {String} Records.Phone  會員電話
 * @apiSuccess {String} Records.Ismember  身分   Y:會員 N:非會員
 * @apiSuccess {String} Records.Salesname  業務員
 * @apiSuccess {String} Records.Inout  '':來電  '撥出':撥出'
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getPhonerecord
 */

router.post('/getPhonerecord', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getPhonerecord request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getPhonerecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getPhonerecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.Token==null||req.body.Token==""){
        finish(1, response, "250","");
    }
    else if((req.body.Startym==null && req.body.Salesid==null && req.body.Qrystr==null)||(req.body.Startym=="" && req.body.Salesid=="" && req.body.Qrystr=="")) {
      finish(1, response, "114","");
    }
    else {
        models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
            if (user!=null){
                var arr=[];
                if (req.body.Qrystr!="")
                    arr.push({Phone:{$like:"%"+req.body.Qrystr+"%"}});
                if (req.body.Startym!=""){
                  if (req.body.Startym=="3days"){
                    var ed=moment();
                    var sd=moment().add(-3,"days");
                    arr.push({$and:[{Calldate:{$gte:sd}},{Calldate:{$lt:ed}}]});
                  }
                  else if (req.body.Startym=="6months"){
                    var ed=moment();
                    var sd=moment().add(-6,"months");
                    arr.push({$and:[{Calldate:{$gte:sd}},{Calldate:{$lt:ed}}]});
                  }
                  else {
                    var startdate=moment(req.body.Startym+"/01","YYYY/MM/DD");
                    var strYM=moment().format("YYYY/MM");
                    var enddate=moment(strYM+"/01","YYYY/MM/DD").add(1,"Months");
                    arr.push({$and:[{Calldate:{$gte:startdate}},{Calldate:{$lt:enddate}}]});
                  }
                }
                else {
                    arr.push({Calldate:{$gte:moment().add(-6,"Months")}});
                }
                if (req.body.Ismember=="Y"){
                    arr.push({MemberId:{$gt:0}})
                }
                else if (req.body.Ismember=="N"){
                    arr.push({MemberId:null})
                }
                if (req.body.Salesid>0){
                    arr.push({SaleId:req.body.Salesid})
                }
                var qryparm={where:{$and:arr}};
                // console.log(JSON.stringify(qryparm));
                models.Phonerecord.count(qryparm).then(function (cnt) {
                    response["Cnt"]=cnt;
                    // qryparm["offset"]=50*req.body.Pageno;
                    // qryparm["limit"]=50;
                    qryparm["include"]=[{model:models.Sales},{model:models.Member}];
                    qryparm["order"]=[["Calldate","DESC"]];
                    models.Phonerecord.findAll(qryparm).then(function (docs) {
                        if (docs==null || docs.length==0){
                            finish(1, response, "103","");
                        }
                        else {
                            for  (var i=0;i<docs.length;++i){
                                var salesname=docs[i].Salesname;
                                var ismember=(docs[i].MemberId!=null)?"Y":"N";
                                var memberstate=(docs[i].Member!=null)?docs[i].Member["State"]:"";
                                // if (memberstate=="N")  console.log(memberstate,docs[i].Phone);
                                var rec={Datestr:moment(docs[i].Calldate).tz("Asia/Taipei").format("YYYY.MM.DD HH:mm:ss"),Phone:docs[i].Phone,Salesname:salesname,Ismember:ismember,Inout:docs[i].Inout,Memberstate:memberstate};
                                response["Records"].push(rec);
                            }
                            finish(0, response, "0","");
                        }
                    })
                })
            }
            else {
                finish(null, response, "100","");
            }
        });
    }
});

function qrySalesname(inpstr,callback) {
    if (inpstr=="")
        callback(null);
    else {
        models.Sales.findAll({where:{Salesname:{$like:"%"+inpstr+"%"}}}).then(function (docs) {
            if (docs==null||docs.length==0)
                callback(null);
            else {
                var arr=[]
                for (var i=0;i<docs.length;++i)
                    arr.push(docs[i].id);
                callback({SaleId:arr});
            }
        })
    }
}

/**
 * @api {post} /adminmgr/getCrashlog  0110.getCrashlog
 * @apiName getCrashlog
 * @apiGroup Admin
 *
 * @apiDescription  消費紀錄管理/  消費紀錄查詢
 *
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Records} Records  消費紀錄清單
 * @apiSuccess {String} Records.Datestr  年月日
 * @apiSuccess {String} Records.UUID  UUID
 * @apiSuccess {String} Records.Logtxt  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getCrashlog 
 */
router.post('/getCrashlog', function (req, res, next) {

    boxlogger.Info('info', '/adminmgr/getCrashlog request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode,ErrField) {
        errors.SetBackErrorResponse(response, ErrCode,ErrField);
        if (err) {
            boxlogger.Error('error', '/adminmgr/getCrashlog response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/adminmgr/getCrashlog response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    models.Crashlog.findAll({order:[["createdAt","DESC"]]}).then(function (docs) {
        if (docs==null || docs.length==0){
            finish(1, response, "103","");
        }
        else {
            for  (var i=0;i<docs.length;++i){
                var rec={ Datestr:moment(docs[i].createdAt).format("YYYY.MM.DD HH:mm:ss"),UUID:docs[i].UUID,Logtxt:docs[i].Logtxt};
                response["Records"].push(rec);
            }
            finish(0, response, "0","");
        }
    })
});

/**
 * @api {post} /adminmgr/getSetting  0111.getSetting
 * @apiName getSetting
 * @apiGroup Admin
 *
 * @apiDescription  時間設定/  時間設定查詢
 *
 * @apiParam {String} Token  Token
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getSetting
 */

router.post('/getSetting', function (req, res, next) {

  boxlogger.Info('info', '/adminmgr/getSetting request: ' + JSON.stringify(req.body));

  var response = {};
  response["Setting"]={};
  var finish = function(err, response, ErrCode,ErrField) {
    errors.SetBackErrorResponse(response, ErrCode,ErrField);
    if (err) {
      boxlogger.Error('error', '/adminmgr/getSetting response: ' + JSON.stringify(response));
    } else {
      boxlogger.Info('info', '/adminmgr/getSetting response: ' + JSON.stringify(response));
    }
    return res.send(response);
  };

  models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
    if (user==null){
      finish(null, response, "100","");
    }
    else{
      models.Setting.findOne({}).then(function (docs) {
        if (docs==null || docs.length==0){
          finish(1, response, "103","");
        }
        else {
          response["Setting"]={Starttime:docs.Starttime,Endtime:docs.Endtime,Cnt:docs.Cnt}
          finish(0, response, "0","");
        }
      })
    }
  })
});

/**
 * @api {post} /adminmgr/setSetting  0112.setSetting
 * @apiName setSetting
 * @apiGroup Admin
 *
 * @apiDescription  setSetting/  setSetting
 *
 * @apiParam {String} Token  Token
 * @apiParam {String} Starttime
 * @apiParam {String} Endtime
 * @apiParam {Number} Cnt
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/setSetting
 */
router.post('/setSetting', function (req, res, next) {

  boxlogger.Info('info', '/adminmgr/setSetting request: ' + JSON.stringify(req.body));

  var response = {};
  var finish = function(err, response, ErrCode,ErrField) {
    errors.SetBackErrorResponse(response, ErrCode,ErrField);
    if (err) {
      boxlogger.Error('error', '/adminmgr/setSetting response: ' + JSON.stringify(response));
    } else {
      boxlogger.Info('info', '/adminmgr/setSetting response: ' + JSON.stringify(response));
    }
    return res.send(response);
  };

  if (req.body.Token==null||req.body.Token==""){
    finish(1, response, "250","");
  }
  else {
    models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
      if (user!=null){
        models.Setting.findOne({}).then(function (docs) {
          if (docs!=null){
            docs.Starttime=  req.body.Starttime;
            docs.Endtime=  req.body.Endtime;
            docs.Cnt=  req.body.Cnt;
            docs.save(function (err) {
            }).then(function () {
              finish(null, response, "0","");
            }).catch(function (err0) {
              finish(null, response, "999",err0.message);
            });
          }
          else {
            finish(1, response, "103","");
          }
        })
      }
      else {
        finish(null, response, "100","");
      }
    });
  }
});

/**
 * @api {post} /adminmgr/getPerformance  0113.getPerformance
 * @apiName getPerformance
 * @apiGroup Admin
 *
 * @apiDescription  業務員績效查詢
 *
 * @apiParam {String} Token  Token
 * @apiParam {String} Startym  起始年月(YYYY/MM)
 * @apiParam {String} Endym  結束年月(YYYY/MM)
 * @apiParam {String} Qrystr   查詢電話號碼
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Cnt  總筆數
 * @apiSuccess {Records} Records  業務員績效紀錄清單
 * @apiSuccess {String} Records.YYYYMM  年月
 * @apiSuccess {String} Records.Salesname  業務員
 * @apiSuccess {Int} Records.Cnt  次數
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 100 此Token無效
 * @apiError 103 查無此資料
 *
 * @apiSampleRequest /adminmgr/getPerformance
 */

router.post('/getPerformance', function (req, res, next) {

  boxlogger.Info('info', '/adminmgr/getPerformance request: ' + JSON.stringify(req.body));

  var response = {};
  response["Records"]=[];
  var finish = function(err, response, ErrCode,ErrField) {
    errors.SetBackErrorResponse(response, ErrCode,ErrField);
    if (err) {
      boxlogger.Error('error', '/adminmgr/getPerformance response: ' + JSON.stringify(response));
    } else {
      boxlogger.Info('info', '/adminmgr/getPerformance response: ' + JSON.stringify(response));
    }
    return res.send(response);
  };

  if (req.body.Token==null||req.body.Token==""){
    finish(1, response, "250","");
  }
  else {
    models.Admin.findOne({where:{Token:req.body.Token}}).then(function (user) {
      if (user!=null){
        //moment().tz("Asia/Taipei").format("YYYYMM")
        var stym=(req.body.Startym==undefined||req.body.Startym=="")?"":req.body.Startym;
        var edym=(req.body.Endym==undefined||req.body.Endym=="")?"":req.body.Endym;
        if (stym=="" && edym==""){
            stym=moment().tz("Asia/Taipei").format("YYYYMM");
            edym=moment().tz("Asia/Taipei").format("YYYYMM");
            // console.log("stym*****"+stym)
        }
        var sql='select DATE_FORMAT(cmsdate,"%Y%m") ym, Salesname,count(*) cnt from Salesrecords where 1=1 ';
        if (stym!="") sql+="and DATE_FORMAT(cmsdate,\"%Y%m\")>='"+stym+"'";
        if (edym!="") sql+="and DATE_FORMAT(cmsdate,\"%Y%m\")<='"+edym+"'";
        if (req.body.Qrystr!="" && req.body.Qrystr!=undefined) sql+=" and UPPER(Salesname) like '%"+req.body.Qrystr.toUpperCase()+"%'";
        // console.log(sql)
        sql+=" group by Salesname,ym order by salesname";
        models.sequelize.query(sql, {type: models.sequelize.QueryTypes.SELECT}).then(function (docs) {
          if (docs==null || docs.length==0){
            finish(1, response, "103","");
          }
          else {
            for  (var i=0;i<docs.length;++i){
              response["Records"].push(docs[i]);
            }
            finish(0, response, "0","");
          }
        });
      }
      else {
        finish(null, response, "100","");
      }
    });
  }
});

module.exports = router;