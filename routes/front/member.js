/**
 * Created by ralph on 7/21/2017.
 */
var models  = require('../../models');
var express = require('express');
var router = express.Router();
//
var Sequelize = require("sequelize");
// util
var util = require('util');
// Random String
var randomstring = require('randomstring');
//Errors
var errors = require('../../lib/errors');
//Errors
var pushNotification = require('../../services/pushService');
//box logger
var boxlogger = require('../../lib/boxloger');
// config
var config = require('config.json')('setting.json');
// moment: date/time format
var moment = require('moment-timezone');
// winston logging
var winston = require('winston');
// password hash
var CryptoJS = require("crypto-js");
var SHA256 = require('crypto-js/sha256');
// generate access token
var randtoken = require('rand-token');
var validator = require('validator');
//userCheck
var tool = require('../../lib/tool');
var request = require("request");
var iconv = require('iconv-lite');

/**
 * @api {post} /member/sendUUID 0101.Member_sendUUID
 * @apiName Member_sendUUID
 * @apiGroup Front
 *
 * @apiDescription  會員登入
 *
 * @apiParam {String} UUID  UUID
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/sendUUID
 */

router.post('/sendUUID', function (req, res, next) {

    boxlogger.Info('info', '/member/sendUUID request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/sendUUID response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/sendUUID response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null){
        finish(1, response, "250");
    }
    else {
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                var sales={UUID:req.body.UUID,Salesname:"",Show1:"N",Show2:"N",Show3:"N",Show4:"N"};
                models.Sales.create(sales).then(function () {
                    finish(0, response, "0");
                })
            }
            else {
                finish(1, response, "101");
            }
        }).catch(function (error) {
            // console.log(error.message);
            finish(1, response, "999");
        })
    }
});

/**
 * @api {post} /member/qryPhonerecord 0102.Member_qryPhonerecord
 * @apiName Member_qryPhonerecord
 * @apiGroup Front
 *
 * @apiDescription  查詢來電的客戶紀錄
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Phone  來電電話(只需傳入後9碼,不含 "-")
 * @apiParam {String} Phonestr  完整來電電話
 * @apiParam {String} Version  Version
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Count  消費總次數
 * @apiSuccess {String} Blacklist  是否為黑名單
 * @apiSuccess {String} Remark  是否顯示會員備註
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/qryPhonerecord
 */

router.post('/qryPhonerecord', function (req, res, next) {

    boxlogger.Info('info', '/member/qryPhonerecord request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/qryPhonerecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/qryPhonerecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null||req.body.Phone==null||req.body.Phonestr==null){
        finish(1, response, "250");
    }
    else {
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                finish(1, response, "102");
            }
            else {
                if (!tool.chkSale(user)){
                  finish(1, response, "113");
                }
                else{
                  models.Member.findOne({where:{Deleteflag:"N",Phone:{$like:"%"+req.body.Phone.replace(/ /g,'')}}}).then(function (doc) {
                    var rec=new models.Phonerecord();
                    rec.Salesname=user.Salesname;
                    rec.SaleId=user.id;
                    rec.Phone=req.body.Phonestr.replace(/ /g,'');
                    rec.Calldate=moment().tz("Asia/Taipei");
                    if (req.body.Version!=null){
                      rec.Version=req.body.Version;
                    }
                    if (doc!=null){
                      rec.MemberId=doc.id;
                      response["Count"]=(user.Show1=="N" )?0:doc.Sum2;
                      if (user.Show2=="Y" && doc.State=="N")  response["Count"]=0;

                      if (user.Show2=="Y")
                        response["Blacklist"]=doc.State;
                      if (user.Show3=="Y")
                        response["Remark"]=doc.Remark;
                    }

                    rec.save().then(function () {
                      if (doc!=null){
                        finish(0, response, "0");
                      }
                      else{
                        finish(0, response, "0");
                        //CR1--1.檢查時間 若非警察執勤時段 加入會員"暫時安全名單"   2.有兩筆跨區資料 加入會員"暫時安全名單"
                        // tool.getSetting(function (doc) {
                        //   var chk=tool.chkPeriod(doc.Starttime,doc.Endtime);
                        //   var cnt=doc.Cnt;
                        //   if (chk){
                        //     models.Member.create({Phone:req.body.Phonestr,Sum1:0,Sum2:0,State:"Y",Remark:"暫時安全名單"}).then(function () {
                        //       finish(0, response, "0");
                        //     });
                        //   }
                        //   else{
                        //     //檢查是否有指定數量跨區資料 setting.cnt
                        //     models.Phonerecord.findAll({where:{Phone:req.body.Phonestr},include:[{model:models.Sales}]}).then(function (docs) {
                        //       if (docs!=null){
                        //         if (tool.chkSameZip(docs,cnt)){
                        //           console.log("********same zipcode****** docs.length**"+docs.length)
                        //           finish(0, response, "0");
                        //         }
                        //         else {
                        //           console.log("*****not same zipcode create 暫時安全名單****")
                        //           models.Member.create({Phone:req.body.Phonestr,Sum1:0,Sum2:0,State:"Y",Remark:"暫時安全名單"}).then(function () {
                        //             finish(0, response, "0");
                        //           });
                        //         }
                        //       }
                        //       else {
                        //         finish(0, response, "0");
                        //       }
                        //     })
                        //   }
                        //
                        // })
                      }

                    })
                  })
                }
            }
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
});

/**
 * @api {post} /member/addCalloutrecord 010205.Member_addCalloutrecord
 * @apiName Member_addCalloutrecord
 * @apiGroup Front
 *
 * @apiDescription  新增撥出電話紀錄
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Phone  撥出電話(只需傳入後9碼,不含 "-")
 * @apiParam {String} Phonestr  完整來電電話
 * @apiParam {String} Version  Version
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/addCalloutrecord
 */

router.post('/addCalloutrecord', function (req, res, next) {

    boxlogger.Info('info', '/member/addCalloutrecord request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/addCalloutrecord response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/addCalloutrecord response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null||req.body.Phone==null||req.body.Phonestr==null){
        finish(1, response, "250");
    }
    else {
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                finish(1, response, "102");
            }
            else {
              if (!tool.chkSale(user)){
                finish(1, response, "113");
              }
              else{
                models.Member.findOne({where:{Phone:req.body.Phonestr}}).then(function(mbr){
                  var mid=(mbr!=null)?mbr.id:null;
                  var rec=new models.Phonerecord();
                  rec.Salesname=user.Salesname;
                  rec.SaleId=user.id;
                  rec.MemberId=mid;
                  rec.Phone=req.body.Phonestr.replace(/ /g,'');
                  rec.Calldate=moment().tz("Asia/Taipei");
                  rec.Inout="撥出";
                  rec.save().then(function () {
                    finish(0, response, "0");
                  })
                })
              }
             }
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
});

/**
 * @api {post} /member/qryMember 0103.Member_qryMember
 * @apiName Member_qryMember
 * @apiGroup Front
 *
 * @apiDescription  會員查詢
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Phone  來電電話(只需傳入後9碼,不含 "-")
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {Int} Sum1  上一個月消費次數
 * @apiSuccess {Int} Sum2  消費總次數
 * @apiSuccess {String} Show1  是否顯示消費紀錄
 * @apiSuccess {String} Show2  業務員是否可看黑名單
 * @apiSuccess {String} State  N:黑名單
 * @apiSuccess {String} Remark  後台備註
 * @apiSuccess {Records} Records[]  消費記錄清單
 * @apiSuccess {String} Datestr  消費年月
 * @apiSuccess {String} Salesname  業務專員
 * @apiSuccess {String} Remark  消費備註
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/qryMember
 */

router.post('/qryMember', function (req, res, next) {

    boxlogger.Info('info', '/member/qryMember request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/qryMember response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/qryMember response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null||req.body.Phone==""||req.body.Phone==null){
        finish(1, response, "250");
    }
    else {
        var chkdate1=getPrevMonth(1);
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                finish(1, response, "102");
            }
            else {
              if (!tool.chkSale(user)){
                finish(1, response, "113");
              }
              else{
                models.Member.findOne({where:{Deleteflag:"N",Phone:{$like:"%"+req.body.Phone.replace(/ /g,'')}},include:[{model:models.Salesrecord}]}).then(function (doc) {
                  if (doc==null){
                    finish(1, response, "103");
                  }
                  else {
                    response["State"]=doc.State;
                    response["Remark"]=doc.Remark;
                    response["Show1"]=user.Show1;
                    response["Show2"]=user.Show2;
                    //
                    var pcnt3=doc.Salesrecords.length, pcnt1=0;
                    var prev1=doc.Salesrecords.filter(function (val) {
                      return val.Cmsdate>=chkdate1[1] && val.Cmsdate<chkdate1[0];
                    })
                    pcnt1=prev1.length;
                    // console.log(doc.Salesrecords.length+"*****"+user.Show1+"    "+pcnt1+"  "+pcnt3);
                    //
                    response["Sum1"]=(user.Show1=="N")?0:pcnt1;// doc.Sum1;
                    response["Sum2"]=(user.Show1=="N")?0:pcnt3;//doc.Sum2;
                    if (user.Show2=="Y" && doc.State=="N"){response["Sum1"]=0;response["Sum2"]=0;}

                    if (user.Show1=="Y"){
                      // finish(0, response, "0");
                      //僅顯示上個月消費紀錄
                      var startdate=moment(moment().add(-1,"Month").format("YYYY-MM")+"-01 00:00:00" ,"YYYY-MM-DD HH:mm:ss");
                      var enddate=moment(moment().format("YYYY-MM")+"-01 00:00:00" ,"YYYY-MM-DD HH:mm:ss");
                      models.Salesrecord.findAll({where:{$and:[{MemberId:doc.id},{Cmsdate:{$gte:startdate}}]},include:[{model:models.Sales}]}).then(function (docs) {
                          if (docs!=null && docs.length>0){
                              cb1cnt=docs.length;
                              for (var i=0;i<docs.length;++i){
                                  var salename=(docs[i].Sale!=null)?docs[i].Sale.Salesname:"";
                                  var sales={Datestr:moment(docs[i].Cmsdate).format("YYYY.MM.DD"),Salesname:salename,Remark:docs[i].Remark};
                                  response["Records"].push(sales);
                                  chkComplete1(response)
                              }
                          }
                          else
                              finish(0, response, "0");
                      })
                    }
                    else
                      finish(0, response, "0");
                  }
                })
              }
            }
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
    var cb1cnt=0;
    function chkComplete1(response) {
        --cb1cnt;
        if (cb1cnt==0){
            finish(0, response, "0","");
        }
    }
});
function getPrevMonth(month) {
  var inpdate=moment().format("YYYY/MM")+"/01";
  var outdate1=moment(inpdate,"YYYY/MM/DD");
  var outdate2=moment(inpdate,"YYYY/MM/DD").add(-month,"Months");
  return [outdate1,outdate2];
}
/**
 * @api {post} /member/qryDailyreport 0104.Member_qryDailyreport
 * @apiName Member_qryDailyreport
 * @apiGroup Front
 *
 * @apiDescription  查詢當日回報列表
 *
 * @apiParam {String} UUID  UUID
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {String} Salesname  業務專員
 * @apiSuccess {String} Show4  是否顯示回報
 * @apiSuccess {Records} Records[]  消費記錄清單
 * @apiSuccess {String} Datestr  消費年月
 * @apiSuccess {String} Phone  客戶電話
 * @apiSuccess {String} Remark  消費備註
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/qryDailyreport
 */

router.post('/qryDailyreport', function (req, res, next) {

    boxlogger.Info('info', '/member/qryDailyreport request: ' + JSON.stringify(req.body));

    var response = {};
    response["Records"]=[];
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/qryDailyreport response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/qryDailyreport response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null){
        finish(1, response, "250");
    }
    else {
        // var ymd=moment().tz("Asia/Taipei").format("YYYY-MM-DD");
        var starttime=moment().add(-1,"Days").format("YYYY-MM-DD HH:mm:ss");
        var endtime=moment().format("YYYY-MM-DD HH:mm:ss");
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                finish(1, response, "102");
            }
            else {
              if (!tool.chkSale(user)){
                finish(1, response, "113");
              }
              else{
                response["Show4"]=user.Show4;
                response["Salesname"]=user.Salesname;
                models.Salesrecord.findAll({where:{$and:[{SaleId:user.id},{Cmsdate:{$gte:moment(starttime)}},{Cmsdate:{$lte:moment(endtime)}}]}}).then(function (docs) {
                  if (docs!=null && docs.length>0 && user.Show4=="Y") {
                    for (var i=0;i<docs.length;++i){
                      var sales={Datestr:moment(docs[i].Cmsdate).format("YYYY.MM"),Phone:docs[i].Phone,Remark:docs[i].Remark};
                      response["Records"].push(sales);
                    }
                  }
                  finish(0, response, "0");
                })
              }
            }
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
});

/**
 * @api {post} /member/saleReport 0105.Member_saleReport
 * @apiName Member_saleReport
 * @apiGroup Front
 *
 * @apiDescription  將消費紀錄回傳至後台
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Phone  客戶電話
 * @apiParam {String} Remark  消費備註
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {String} Show4  是否顯示回報
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/saleReport
 */


router.post('/saleReport', function (req, res, next) {

    boxlogger.Info('info', '/member/saleReport request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/saleReport response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/saleReport response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null){
        finish(1, response, "250");
    }
    else if (req.body.Phone.length<9){
        finish(1, response, "202");
    }
    else {
        var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
        models.Sales.findOne(qryparm).then(function (user) {
            if (user==null||user.length==0){
                finish(1, response, "102");
            }
            else {
              if (!tool.chkSale(user)){
                finish(1, response, "113");
              }
              else{
                response["Show4"]=user.Show4;
                if (user.Show4=="Y"){
                  //檢查10秒之內是否有紀錄
                  tool.chkSalesrecord(req.body.Phone.replace(/ /g,''),user.id,function (dup) {
                    if (dup){
                      finish(0, response, "0");
                    }
                    else {
                      getMemberid(req.body.Phone.replace(/ /g,''),function (memberid) {
                        var sale={Salesname:user.Salesname,Phone:req.body.Phone.replace(/ /g,''),Cmsdate:moment(),Remark:req.body.Remark,MemberId:memberid,SaleId:user.id};
                        models.Salesrecord.create(sale).then(function () {
                          finish(0, response, "0");
                        })
                      })
                    }
                  })
                  // getMemberid(req.body.Phone.replace(/ /g,''),function (memberid) {
                  //     var sale={Salesname:user.Salesname,Phone:req.body.Phone.replace(/ /g,''),Cmsdate:moment(),Remark:req.body.Remark,MemberId:memberid,SaleId:user.id};
                  //     models.Salesrecord.create(sale).then(function () {
                  //         finish(0, response, "0");
                  //     })
                  // })
                }
                else
                  finish(1, response, "107");
              }
            }
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
});

function getMemberid(phone,callback) {
    models.Member.findOne({where:{Deleteflag:"N",Phone:{$like:"%"+phone}}}).then(function (doc) {
        if (doc!=null){
            doc["Sum2"]+=1;
            doc.save().then(function (doc) {
                callback(doc.id);
            })
        }
        else {
            models.Member.create({Phone:phone,Sum1:0,Sum2:1,State:"Y"}).then(function (doc) {
                callback(doc.id);
            })
        }
    })
}

/**
 * @api {post} /member/sendLog 0106.Member_sendLog
 * @apiName Member_sendLog
 * @apiGroup Front
 *
 * @apiDescription  錯誤紀錄回傳至後台
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Logtxt  Exceptionlog
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/sendLog
 */

router.post('/sendLog', function (req, res, next) {

    boxlogger.Info('info', '/member/sendLog request: ' + JSON.stringify(req.body));

    var response = {};
    var finish = function(err, response, ErrCode) {
        errors.SetErrorResponse(response, ErrCode);
        if (err) {
            boxlogger.Error('error', '/member/sendLog response: ' + JSON.stringify(response));
        } else {
            boxlogger.Info('info', '/member/sendLog response: ' + JSON.stringify(response));
        }
        return res.send(response);
    };

    if (req.body.UUID==""||req.body.UUID==null){
        finish(1, response, "250");
    }
    else {
        models.Crashlog.create({UUID:req.body.UUID,Logtxt:req.body.Logtxt}).then(function () {
            finish(0, response, "0");
        }).catch(function (error) {
            console.log(error.message);
            finish(1, response, "999");
        })
    }
});

//***********CR1************
/**
 * @api {post} /member/qryPhonerecordCR1 0107.Member_qryPhonerecordCR1
 * @apiName Member_qryPhonerecordCR1
 * @apiGroup CR1-Front
 *
 * @apiDescription  查詢來電的客戶紀錄CR1
 *
 * @apiParam {String} UUID  UUID
 * @apiParam {String} Phone  來電電話(只需傳入後9碼,不含 "-")
 * @apiParam {String} Phonestr  完整來電電話
 * @apiParam {String} Version  Version
 * @apiParam {Number} isadd  (來電:true  查詢:false)
 *
 * @apiSuccess {Number} ErrCode 錯誤代碼
 * @apiSuccess {String} ErrMsg  錯誤訊息
 * @apiSuccess {String} Count  消費次數代號(0：消費0次  1：消費1次  1+：消費2~10次  2+：消費11~99次  3+：消費100次以上)
 * @apiSuccess {String} Blacklist  是否為黑名單 (OK NO 0)
 *
 * @apiError 0 成功
 * @apiError 999 系統維護中
 * @apiError 102 此UUID無效
 * @apiError 250 輸入參數不可空白
 *
 * @apiSampleRequest /member/qryPhonerecordCR1
 */

router.post('/qryPhonerecordCR1', function (req, res, next) {

  boxlogger.Info('info', '/member/qryPhonerecordCR1 request: ' + JSON.stringify(req.body));

  var response = {Blacklist:"0",Count:"0"};
  var finish = function(err, response, ErrCode) {
    errors.SetErrorResponse(response, ErrCode);
    if (err) {
      boxlogger.Error('error', '/member/qryPhonerecordCR1 response: ' + JSON.stringify(response));
    } else {
      boxlogger.Info('info', '/member/qryPhonerecordCR1 response: ' + JSON.stringify(response));
    }
    return res.send(response);
  };

  // console.log("*********qryPhonerecordCR1************")
  if (req.body.UUID==""||req.body.UUID==null||req.body.Phone==null||req.body.Phonestr==null){
    finish(1, response, "250");
  }
  else {
    var  qryparm={where:{UUID:req.body.UUID,State:"Y"}};
    models.Sales.findOne(qryparm).then(function (user) {
      if (user==null||user.length==0){
        finish(1, response, "102");
      }
      else {
        if (!tool.chkSale(user)){
          finish(1, response, "113");
        }
        else{
          models.Sales.findAll({}).then(function(allsales){
            var starttime=moment().add(-1,"Days").format("YYYY-MM-DD HH:mm:ss");
            var endtime=moment().format("YYYY-MM-DD HH:mm:ss");
            models.Salesrecord.findAll({where:{$and:[{Phone:req.body.Phonestr},{Cmsdate:{$gte:moment(starttime)}},{Cmsdate:{$lte:moment(endtime)}}]}}).then(function(allrecs){
              //models.Member.findOne({where:{Deleteflag:"N",Phone:{$like:"%"+req.body.Phone.replace(/ /g,'')}}}).then(function (doc) {  //20191122修改
              models.Member.findOne({where:{Deleteflag:"N",Phone:req.body.Phonestr}}).then(function (doc) {
                var rec=new models.Phonerecord();
                rec.Salesname=user.Salesname;
                rec.SaleId=user.id;
                rec.Phone=req.body.Phonestr.replace(/ /g,'');
                rec.Calldate=moment().tz("Asia/Taipei");
                if (req.body.Version!=null){
                  rec.Version=req.body.Version;
                }
                if (doc!=null){
                  rec.MemberId=doc.id;
                  response["Count"]=(user.Show1=="N" )?0:doc.Sum2;
                  if (user.Show2=="Y" && doc.State=="N")  response["Count"]=0;

                  if (user.Show2=="Y")
                    response["Blacklist"]=doc.State;
                  if (user.Show3=="Y")
                    response["Remark"]=doc.Remark;
                  response["Count"]=setNumberstr(response["Count"]);
                  response["Blacklist"]=setStatestr(response["Blacklist"]);
                  response["Records"]=[];
                  if (allrecs.length>0){
                    allrecs.forEach(function(val){
                      var sales=allsales.filter(function(s){
                        return s.id==val.SaleId;
                      })
                      var salename=(sales.length>0)?sales[0].Salesname:"";
                      response["Records"].push({Datestr:moment(val.Cmsdate).format("YYYY.MM"),Sales:salename,Remark:val.Remark});
                    })
                  }
                }
                issave(rec,req.body.isadd,function () {
                  if (doc!=null){
                    finish(0, response, "0");
                  }
                  else{
                    //CR1--1.檢查時間 若非警察執勤時段 加入會員"暫時安全名單"   2.有兩筆跨區資料 加入會員"暫時安全名單"
                    tool.getSetting(function (setting) {
                      var cnt=setting.Cnt;
                      //20190902改成3個月
                      var sd=new moment().add(-3,"months");
                      // var sd=new moment().add(-1,"months");
                      //檢查是否有指定數量跨區資料 setting.cnt
                      models.Phonerecord.findAll({where:{Phone:req.body.Phonestr,createdAt:{$gte:sd},Inout:""},include:[{model:models.Sales}]}).then(function (docs) {
                        if (docs!=null){
                          if (tool.chkSameZip(docs,cnt)){
                            var now=new moment().tz("Asia/Taipei").format("YYYYMMDDHHmmss");
                            // console.log("******CR1--1.檢查時間******")
                            var chk=tool.chkPeriod(now,setting.Starttime,setting.Endtime);
                            if (chk && req.body.isadd==1){ //isadd!=1代表查詢
                              response = {Blacklist:"OK",Count:"0",Remark:"暫時安全名單"};
                              finish(0, response, "0");
                            }
                            else {
                              for (var i=0;i<docs.length;++i){
                                now=new moment(docs[i].createdAt).tz("Asia/Taipei").format("YYYYMMDDHHmmss");
                                chk=tool.chkPeriod(now,setting.Starttime,setting.Endtime);
                                if (chk) break;
                              }
                              if (chk){
                                response = {Blacklist:"OK",Count:"0",Remark:"暫時安全名單"};
                                finish(0, response, "0");
                              }
                              else{
                                finish(0, response, "0");
                              }
                            }
                          }
                          else {
                            // console.log("*****not same zipcode create 暫時安全名單****");
                            response = {Blacklist:"OK",Count:"0",Remark:"暫時安全名單"};
                            finish(0, response, "0");
                          }
                        }
                        else {
                          finish(0, response, "0");
                        }
                      })
                      // var chk=tool.chkPeriod(setting.Starttime,setting.Endtime);
                      //
                      // if (chk){
                      //   response = {Blacklist:"OK",Count:"0",Remark:"暫時安全名單"};
                      //   finish(0, response, "0");
                      //   // models.Member.create({Phone:req.body.Phonestr,Sum1:0,Sum2:0,State:"Y",Remark:"暫時安全名單"}).then(function () {
                      //   //   response = {Blacklist:"OK",Count:"0"};
                      //   //   finish(0, response, "0");
                      //   // });
                      // }
                      // else{
                      //   //檢查是否有指定數量跨區資料 setting.cnt
                      //   models.Phonerecord.findAll({where:{Phone:req.body.Phonestr},include:[{model:models.Sales}]}).then(function (docs) {
                      //     if (docs!=null){
                      //       if (tool.chkSameZip(docs,cnt)){
                      //         console.log("********same zipcode****** docs.length**"+docs.length)
                      //         finish(0, response, "0");
                      //       }
                      //       else {
                      //         console.log("*****not same zipcode create 暫時安全名單****")
                      //         response = {Blacklist:"OK",Count:"0",Remark:"暫時安全名單"};
                      //         finish(0, response, "0");
                      //         // models.Member.create({Phone:req.body.Phonestr,Sum1:0,Sum2:0,State:"Y",Remark:"暫時安全名單"}).then(function () {
                      //         //   response = {Blacklist:"OK",Count:"0"};
                      //         //   finish(0, response, "0");
                      //         // });
                      //       }
                      //     }
                      //     else {
                      //       finish(0, response, "0");
                      //     }
                      //   })
                      // }

                    })
                  }
                })

                function issave(rec,add,cb) {
                  if(add==1){
                    rec.save().then(function () {
                      cb("");
                    })
                  }
                  else {
                    cb("");
                  }
                }
                // rec.save().then(function () {
                //
                // })
              })
            })
          })
        }
      }
    }).catch(function (error) {
      console.log(error.message);
      finish(1, response, "999");
    })


  }
});

function setNumberstr(cnt) {
  var res=cnt.toString();
  if (cnt>=100)
    res="3+";
  else if (cnt>11)
    res="2+";
  else if (cnt>1)
    res="1+";
  return res;
}
function setStatestr(state) {
  var res="0";
  if (state=="Y")
    res="OK";
  else if (state=="N")
    res="NO";
  return res;
}

module.exports = router;