var express = require('express');
var config = require('config.json')('setting.json');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('admin/business', {pageTitle: config.views.admin.pageTitle});
});

module.exports = router;