/**
 * Created by ralph on 7/21/2017.
 */
var models  = require('../models');
var express = require('express');
var router = express.Router();
var config = require('config.json')('setting.json');
var CryptoJS = require("crypto-js");
var SHA256 = require('crypto-js/sha256');
// util
var util = require('util');
// config
var config = require('config.json')('setting.json');
var moment = require('moment-timezone');
var tool = require('../lib/tool');
var request = require("request");
var iconv = require('iconv-lite');

router.post('/test', function(req, res) {
    var hash = SHA256(req.body.Password);
    var pswd=hash.toString(CryptoJS.enc.Base64);
    res.json({message:pswd});
});

module.exports = router;