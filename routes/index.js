var models  = require('../models');
var express = require('express');
var router  = express.Router();
var test=require("../routes/test");

router.get('/', function(req, res) {
  models.User.findAll({
    include: [ models.Task ]
  }).then(function(users) {
    res.render('index', {
      title: 'Sequelize: Express Example',
      users: users
    });
  });
});

router.post('/index2', function(req, res) {
    res.redirect("http://www.Driver.com:7761/front/setting.html?status="+req.body.Status+"&tradeinfo="+req.body.TradeInfo)
});

module.exports = router;
