var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();

var BACK = '../public/admin/',
    MAIN_JS = '../public/javascripts/',
    FRONT = '../public/front/',
    MAIN_CSS = '../public/stylesheets/';

/*-----Backstage jade-----*/
var compileSASS = function (filename, option) {
    return sass('scss/admin/*.scss', option)
        .pipe(autoprefixer('last 2 version','> 5%'))
        .pipe(concat(filename))
        .pipe(gulp.dest(BACK + 'css'))
        .pipe(gulp.dest(MAIN_CSS))
        .pipe(browserSync.stream());
};

gulp.task('custom-js', function () {
    return gulp.src([
        'example/src/js/helpers/*.js',
        'example/src/js/*js'
    ]).pipe(concat('custom.js'))
        .pipe(gulp.dest(MAIN_JS))
        .pipe(rename({suffix:'.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(MAIN_JS))
        .pipe(browserSync.stream());
});

gulp.task('else-js', function () {
    return gulp.src(['javascript/*.js'])
        .pipe(gulp.dest(MAIN_JS))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function () {
    return gulp.src(['javascript/admin/*.js'])
        .pipe(gulp.dest(BACK + '/js'))
        .pipe(browserSync.stream());
});

gulp.task('sass',function () {
    return compileSASS('style.css', {});
});

gulp.task('browser-sync', function () {
    browserSync.init({
        server:{
            baseDir: '../'
        },
        startPath: '../public/admin/login.html'
    })
});

gulp.task('watch', function () {
    // Watch .html file
    gulp.watch('./admin/*.html', browserSync.reload);
    // Watch .js file
    gulp.watch('./javascript/admin/*.js', ['scripts']);
    // Watch else .js file
    gulp.watch('./javascript/*.js',['else-js']);
    //Watch custom.js file
    gulp.watch('./example/src/js/custom.js', ['custom-js']);
    // Watch .scss file
    gulp.watch('./scss/admin/**/*.scss', ['sass']);
});

gulp.task('default', ['build-copy', 'watch']);  //'browser-sync', 'templates'

var jade = require('gulp-jade');

gulp.task('templates', function () {
    const YOUR_LOCALS = {pageTitle: 'callList - 後台管理系統'};
    gulp.src('../views/admin/*.jade')
        .pipe(jade({
            locals : YOUR_LOCALS
        }))
        .pipe(gulp.dest('../public/admin'));
    gulp.src('../views/*.jade')
        .pipe(jade())
        .pipe(gulp.dest('../public'));
});

gulp.task('build-copy',function () {
    return gulp.src(['example/production/**/*', '!example/production/*.html'])
        .pipe(gulp.dest('../public/admin'))
});
/*--------------------------------------------------*/
