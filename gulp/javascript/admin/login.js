function login(act, pwd) {
    // if (!act) return mDialog.Check('帳號不可為空');
    if (!pwd) return mDialog.Check('密碼不可為空');

    WebAPI.loginAdmin(pwd, function (data) {
        if (data.ErrCode == 0){
            sessionStorage['token'] = data.Token;
            window.location.href = 'business';
        }else {
            mDialog.Check(data.ErrMsg);
        }
    });


}

$(function () {
    if (sessionStorage.length > 0){
        sessionStorage.clear();
    }

    $('.btn-login').click(function () {
        var act = $('#inputAct').val(), pwd = $('#inputPwd').val();
        login(act, pwd);
    });

    $('body').keypress(function (e) {
        if (e.keyCode == 13){
            $('.btn-login').trigger('click');
        }
    });
});