var mBusinessOpts = {
    getList: function (callback) {
        WebAPI.getSaleslist(mUser.Token, '', function (data) {
            if (data.ErrCode == 0){
                callback(data.Sales);
            }
        });
    },
    setOptList: function () {
        this.getList(function (data) {
            $.each(data, function (i, item) {
                $('#searchType').append('<option value="'+item.Salesid+'">'+item.Salesname+'</option>');
            });
        });
    },
    init: function () {
        this.setOptList();
    }
};

var mMemberInfo = {
    id: mHandle.split(window.location.search.replace('?'), 0),
    phone: mHandle.split(window.location.search.replace('?'), 1),
    startDate: '',
    endDate: '',
    saleID: '',
    saleName: '',
    search: '',
    getList: function (callback) {
        WebAPI.getMbrrecord(mUser.Token, this.id, this.startDate, this.endDate, this.saleName, this.search, function (data) {
            if (data.ErrCode == 0){
                callback(data);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    getListItem: function (data) {
        return [data.Datestr, data.Salesname, data.Remark];
    },
    setSearchOpts: function () {
        var info = this;
        // var $select = $('#searchType');
        var $select = $('#searchBusiness');
        var $dateStart = $('#searchDateS'), $dateEnd = $('#searchDateE');
        info.search = $('#searchInput').val();
        if ($select.is(':disabled')){
            // info.saleID = '';
            info.saleName = '';
        }else{
            // info.saleID = $select.val() ? parseInt($select.val()) : '';
            info.saleName = $select.val() ? $select.val() : '';
        }

        if ($dateStart.is(':disabled') || $dateEnd.is(':disabled')){
            info.startDate = '';
            info.endDate = '';
        }else{
            info.startDate = $dateStart.val();
            info.endDate = $dateEnd.val();
        }
    },
    saveMbRemark: function (remark) {
        WebAPI.setMbrremark(mUser.Token, this.id, remark, function (data) {
            if (data.ErrCode == 0){
                mDialog.Check("儲存成功");
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    init: function ($db) {
        this.getList(function (data) {
            var separatedList = mHandle.separatedData.getData(data.Records);
            $('#remarkInput').val(data.Mbrremark);
            $('#costTotal').text(data.Records.length);

            //Test
            // var separatedList = mHandle.separatedData.getData(testJSON.getData());
            // $('#costTotal').text(testJSON.getData().length);

            mHandle.separatedData.setLayout(separatedList, $db);
        });
    }
};

$(function () {
    // mBusinessOpts.init();

    var $db = $('#dbMember').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        // ordering: false,
        order: [[0, 'desc']],
        columnDefs: [{
            targets: [0,1,2],
            orderable: false
        }],
        searching: dataDB.isSearch,
        info: dataDB.isInfo
    });

    $('.title-page').text(mMemberInfo.phone);
    mMemberInfo.init($db);

    $('#searchBtn').click(function () {
        mMemberInfo.setSearchOpts();
        // console.log("mMemberInfo: ", mMemberInfo);
        mMemberInfo.init($db);
    });
    $('#searchInput').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });
    $('#searchBusiness').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });

    $('#remarkBtn').click(function () {
        mMemberInfo.saveMbRemark($('#remarkInput').val());
    });
    $('#remarkInput').keypress(function (e) {
        if (e.keyCode == 13){
            $('#remarkBtn').trigger('click');
        }
    });
});