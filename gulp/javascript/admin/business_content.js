var mBusinessDetail = {
    info: JSON.parse(sessionStorage['saleInfo']),
    init: function () {
        var info = this.info;
        $('#uuidTitle').text(info.uuid);
        $('#inputTitle').val(info.saleName);
        $('.switch-block').each(function (i, item) {
            var role = $(item).attr('role');
            switch (role){
                case "cost":
                    info.isCost == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "black":
                    info.isBlack == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "remark":
                    info.isRemark == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "report":
                    info.isReport == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
            }
        });
    },
    setStatus: function (type, status) {
        var isOpen = status == 'open' ? 'Y' : 'N';
        switch (type){
            case "cost":
                this.info.isCost = isOpen;
                break;
            case "black":
                this.info.isBlack = isOpen;
                break;
            case "remark":
                this.info.isRemark = isOpen;
                break;
            case "report":
                this.info.isReport = isOpen;
                break;
        }
    },
    saveInfo: function () {
        // console.log(mBusinessDetail.info);
        WebAPI.setSalesinfo(mUser.Token, this.info.id, this.info.saleName, this.info.isCost, this.info.isBlack, this.info.isRemark, this.info.isReport, function (data) {
            if (data.ErrCode == 0){
                mDialog.Check("儲存成功");
                setTimeout(function () {
                    window.location.href = "business";
                }, 1500);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    }
};

$(function () {
    mBusinessDetail.init();

    $('.switch-block').on('click', '.btn-switch', function () {
        var $btnS = $(this);
        var $switchBtn = $btnS.parent();
        var $switchs = $switchBtn.find('.btn-switch');
        $switchs.each(function (i, btn) {
            $(btn).removeClass('selected');
        });
        $btnS.addClass('selected');
        mBusinessDetail.setStatus($switchBtn.attr('role'), $btnS.attr('role'));
    });

    $('.save-block').click(function () {
        mBusinessDetail.saveInfo();
    });

    $('#inputTitle').change(function () {
        mBusinessDetail.info.saleName = this.value;
    });

});