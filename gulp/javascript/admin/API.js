var WebAPI = WebAPI || {
        TAG: "WebAPI.js",
        APP_TYPE: "WEB",
        BASE_URL: "http://13.113.64.25:3002/",
        // BASE_URL: "http://192.168.0.41:3002/",
        COMMON_BASE_URL: "http://13.113.64.25:3002/",
        LOG_2_CONSOLE: true,
        METHOD_TYPE: {
            POST: "POST",
            GET: "GET"
        },

        //0101.loginAdmin
        loginAdmin: function (Password, callbackS) {
            var api = "adminmgr/loginAdmin",
                params = {
                    Password: Password
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0102.getSaleslist
        getSaleslist: function (Token, Qrystr, callbackS) {
            var api = "adminmgr/getSaleslist",
                params = {
                    Token: Token,
                    Qrystr: Qrystr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0103.setSalesinfo
        setSalesinfo: function (Token, Salesid, Salesname, Show1, Show2, Show3, Show4, callbackS) {
            var api = "adminmgr/setSalesinfo",
                params = {
                    Token: Token,
                    Salesid: Salesid,
                    Salesname: Salesname,
                    Show1: Show1,
                    Show2: Show2,
                    Show3: Show3,
                    Show4: Show4
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //010305.deleteSales
        deleteSales: function (Token, Salesid, callbackS) {
            var api = "adminmgr/deleteSales",
                params = {
                    Token: Token,
                    Salesid: Salesid
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0104.getMemberlist
        getMemberlist: function (Token, State, Qrystr, callbackS) {
            var api = "adminmgr/getMemberlist",
                params = {
                    Token: Token,
                    State: State,
                    Qrystr: Qrystr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //010405.setMbrstate
        setMbrstate: function (Token, Memberid, State, callbackS) {
            var api = "adminmgr/setMbrstate",
                params = {
                    Token: Token,
                    Memberid: Memberid,
                    State: State
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //010408.deleteMember
        deleteMember: function (Token, Memberid, callbackS) {
            var api = "adminmgr/deleteMember",
                params = {
                    Token: Token,
                    Memberid: Memberid
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0105.setMbrremark
        setMbrremark: function (Token, Memberid, Remark, callbackS) {
            var api = "adminmgr/setMbrremark",
                params = {
                    Token: Token,
                    Memberid: Memberid,
                    Remark: Remark
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0106.getMbrrecord
        getMbrrecord: function (Token, Memberid, Startym, Endym, Salesname, Qrystr, callbackS) {
            var api = "adminmgr/getMbrrecord",
                params = {
                    Token: Token,
                    Memberid: Memberid,
                    Startym: Startym,
                    Endym: Endym,
                    Salesname: Salesname,
                    Qrystr: Qrystr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0107.getSalesrecord
        getSalesrecord: function (Token, Memberid, Startym, Endym, Salename, Qrystr, callbackS) {
            var api = "adminmgr/getSalesrecord",
                params = {
                    Token: Token,
                    Memberid: Memberid,
                    Startym: Startym,
                    Endym: Endym,
                    Salename: Salename,
                    Qrystr: Qrystr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //010705.deleteSalesrecord
        deleteSalesrecord: function (Token, Arr, callbackS) {
            var api = "adminmgr/deleteSalesrecord",
                params = {
                    Token: Token,
                    Arr: Arr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0108.importSalesrecord
        importSalesrecord: function (Token, Data, callbackS) {
            var api = "adminmgr/importSalesrecord",
                params = {
                    Token: Token,
                    Data: Data
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },
        //0109.getPhonerecord
        getPhonerecord: function (Token, Memberid, Startym, Ismember, Salesid, Qrystr, callbackS) {
            var api = "adminmgr/getPhonerecord",
                params = {
                    Token: Token,
                    Memberid: Memberid,
                    Startym: Startym,
                    Ismember: Ismember,
                    Salesid: Salesid,
                    Qrystr: Qrystr
                };

            this.AjaxCall(this.BASE_URL + api, params, this.METHOD_TYPE.POST, function (data) {
                callbackS(data);
            });
        },

        //All ajax call to server using this method.
        AjaxCall: function (url, obj, methodType, callback, elseopts) {
            var apiCallType, apiName = url.replace(this.BASE_URL, "");
            var isLoadingShow = true;
            if (elseopts){
                //Check the loading type.
                isLoadingShow = elseopts.loadingType != "hide";
            }

            apiCallType = "application/json; charset=utf-8";
            switch (methodType){
                case "GET":
                    break;
                case "POST":
                    obj = JSON.stringify(obj);
                    break;
            }

            // console.log("apiName:", apiName);
            // console.log("url:", url);
            // console.log("obj:", obj);

            $.ajax({
                url: url,
                type: methodType,
                contentType: apiCallType,
                data: obj,
                dataType: "json",
                success: function (data) {
                    if (data.ErrCode == 100){
                        setTimeout(function () {
                            mDialog.Check(data.ErrMsg);
                            localStorage.clear();
                            location.href = "login";
                        }, 1500);
                    }else{
                        callback(data);
                        // console.log('response: ', data);
                    }
                },
                error: function (data) {
                    // console.error('error time: ', new Date());
                    console.error("ajax call error: ", data );
                    // mDialog.Check("系統異常，請聯絡工程師");
                    callback('ajaxError');
                },
                beforeSend: function () {
                    switch (apiName){
                        case "adminmgr/getSaleslist":
                            mLoading.start();
                            break;
                        case "adminmgr/getMemberlist":
                            mLoading.start();
                            break;
                        case "adminmgr/getMbrrecord":
                            mLoading.start();
                            break;
                        case "adminmgr/getPhonerecord":
                            mLoading.start();
                            break;
                        case "adminmgr/getSalesrecord":
                            mLoading.start();
                            break;
                    }
                },
                complete: function () {
                    switch (apiName){
                        case "adminmgr/getSaleslist":
                            mLoading.complete();
                            break;
                        case "adminmgr/getMemberlist":
                            mLoading.complete();
                            break;
                        case "adminmgr/getMbrrecord":
                            mLoading.complete();
                            break;
                        case "adminmgr/getPhonerecord":
                            mLoading.complete();
                            break;
                        case "adminmgr/getSalesrecord":
                            mLoading.complete();
                            break;
                    }
                }
            })
        }
    }