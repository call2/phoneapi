var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var nocache = require('nocache');

//frontapi
var routes = require('./routes/index');
var test  = require('./routes/test');
var member  = require('./routes/front/member');
//
//backapi
var adminmgr=require('./routes/back/adminmgr');
//schedule
var schedule=require('./services/schedule');
//

/*--- views - admin ---*/
var viewALogin = require('./routes/views/admin/login');
var viewABusiness = require('./routes/views/admin/business');
var viewABusinessContent = require('./routes/views/admin/business_content');
var viewAMember = require('./routes/views/admin/member');
var viewAMemberContent = require('./routes/views/admin/member_content');
var viewAConsumerRecord = require('./routes/views/admin/consumer_record');
var viewACallRecord = require('./routes/views/admin/call_record');
var viewASetting = require('./routes/views/admin/setting');
var viewAPerformance = require('./routes/views/admin/performance');
/*-------------------*/

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(nocache());

// enable CORS - For 網頁連接API時在不同網域下, 需要加的function,
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//frontapi--use
app.use('/', routes);
app.use('/test', test);
app.use('/member', member);
//
//backapi--use
app.use('/adminmgr',adminmgr);
//
/*--- views - admin ---*/
app.use('/admin/login', viewALogin);
app.use('/admin/business', viewABusiness);
app.use('/admin/business_content', viewABusinessContent);
app.use('/admin/member', viewAMember);
app.use('/admin/member_content', viewAMemberContent);
app.use('/admin/consumer_record', viewAConsumerRecord);
app.use('/admin/call_record', viewACallRecord);
app.use('/admin/setting', viewASetting);
app.use('/admin/performance', viewAPerformance);
/*--------------------*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: (app.get('env') === 'development') ? err : {}
  });
});

//schedule
schedule.Start();

module.exports = app;
