var ziplist=tools.zipList();
var mBusiness = {
    searchTxt: '',
    pageNo: 0,
    getList: function (callback) {
        WebAPI.getSaleslist(mUser.Token, this.searchTxt, function (data) {
            if (data.ErrCode == 0 || data.ErrCode == 103){
                if (data.ErrCode == 103) mDialog.Check(data.ErrMsg);
                mBusiness.searchData = data.Sales;
                callback(data);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    getListItem: function (data) {
        var checkBox = '<input id="_select_'+data.Salesid+'" type="checkbox" class="hide" name="selectBox"><label for="_select_'+data.Salesid+'" class="select-check"></label>';
        var isCost = data.Show1 == 'Y' ? '<i class="fa fa-check">' : '';
        var isBlack = data.Show2 == 'Y' ? '<i class="fa fa-check">' : '';
        var isRemark = data.Show3 == 'Y' ? '<i class="fa fa-check">' : '';
        var isReport = data.Show4 == 'Y' ? '<i class="fa fa-check">' : '';
        // var area=getArea(data.Zip);
        var editBtn = '<button class="btn btn-edit" data-uuid="'+data.UUID+'" data-id="'+data.Salesid+'" data-salename="'+data.Salesname+'" ' +
            'data-iscost="'+data.Show1+'" data-isblack="'+data.Show2+'" data-isremark="'+data.Show3+'" data-isreport="'+data.Show4+'"  data-Zip="'+data.Zip+'">編輯</button>';
        return [checkBox, data.UUID, data.Salesname ,data.Zip, isCost, isBlack, isRemark, isReport, editBtn];
    },
    deleteSales: function (id, callback) {
        mLoading.start();
        WebAPI.deleteSales(mUser.Token, id, function (data) {
            if (data.ErrCode == 0){
                callback();
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    init: function ($db) {
        this.getList(function (data) {
            var list = [];
            var dataList = data.Sales;
            if (dataList){
                if (dataList.length > 0) {
                    $.each(dataList, function (i, item) {
                        list.push(mBusiness.getListItem(item));
                    });
                }
            }

            $db
                .clear()
                .rows
                .add(list)
                .draw();
        });
    }
};

function getArea(zip) {
  var res="";var area;
  if (zip!=""){
    for (var i=0;i< ziplist.length;++i){
      area= ziplist[i].districts.filter(function (dd) {
        return dd.zip==zip;
      })
      if (area.length>0){
        res=ziplist[i].name+" "+area[0].name;
        break;
      }
    }
  }
  return res;
}

var testJSON = {
    total: 1000,
    pageCount: 50,
    getData: function () {
        var data = [];
        for (var i=0; i < this.total; i++){
            var isCost = i % 10 > 5;
            var isBlack = i % 5 == 1;
            var isRemark = i % 8 < 5;
            var isReport = i % 3 == 2;

            data.push({
                id: "0d4ba4fb-382e" + i,
                businessName: "業務" + i,
                isCost: isCost ? 'Y' : 'N',
                isBlack: isBlack ? 'Y' : 'N',
                isRemark: isRemark ? 'Y' : 'N',
                isReport: isReport ? 'Y' : 'N'
            })
        }
        return data;
    }
};

$(function () {
    var $db = $('#dbBusiness').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        ordering: false,
        // order: [[1, 'desc']],
        // columnDefs: [{
        //     targets: [0,1,2,3,4,5,6,7],
        //     orderable: false
        // }],
        searching: true,
        info: dataDB.isInfo
    }).on('click', '.btn-edit', function () {
        var saleInfo = {
            id: $(this).data('id'),
            saleName: $(this).data('salename'),
            isCost: $(this).data('iscost'),
            isBlack: $(this).data('isblack'),
            isRemark: $(this).data('isremark'),
            isReport: $(this).data('isreport'),
            uuid: $(this).data('uuid'),
            Zip: $(this).data('zip')
        };
        sessionStorage['saleInfo'] = JSON.stringify(saleInfo);
        window.location.href="business_content";
    });

    // var testData = testJSON.getData();
    // var testList = [];
    // var pageList = [];
    // $.each(testData, function (i, item) {
    //     testList.push(mBusiness.getListItem(item));
    //     if ((i+1) % testJSON.pageCount == 0) {
    //         pageList.push(testList);
    //     }
    // });

    // mLoading.start();
    // $db
    //     .clear()
    //     .rows
    //     .add(testList)
    //     .draw();
    // mLoading.complete();

    // var page = 0;
    // var handle = setInterval(function () {
    //     if (page == 0) {
    //         $db.clear().draw();
    //         mLoading.start();
    //     }
    //     if (page >= pageList.length){
    //         clearInterval(handle);
    //         mLoading.complete();
    //         return;
    //     }
    //     console.log('Draw DB count:', page);
    //     $db
    //         .rows
    //         .add(pageList[page])
    //         .draw();
    //     page++;
    // }, 2000);
    mBusiness.init($db);

    $('.delete-block').click(function () {
        var $checked = $('input[name="selectBox"]:checked');
        var checkTotal = $checked.length;
        var checkCount = 0;
        if (checkTotal > 0){
            mDialog.Confirm("確定刪除業務員?", "", function (dialog) {
                dialog.close();
                $checked.each(function (i, item) {
                    var id = $(item).attr('id').replace('_select_', '');
                    mLoading.start();
                    mBusiness.deleteSales(id, function () {
                        checkCount++;
                        if (checkCount == checkTotal){
                            var isComplete = mLoading.complete();
                            if (isComplete){
                                mBusiness.init($db);
                            }
                        }
                    });
                });
            })
        }else{
            mDialog.Check("請先勾選欲刪除的項目");
        }
    });

});