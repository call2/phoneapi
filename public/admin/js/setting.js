
$(function () {

  WebAPI.getSetting(mUser.Token,  function (data) {
    console.log(data)
    if (data.ErrCode == 0 || data.ErrCode == 103){
      if (data.ErrCode == 103)
        mDialog.Check(data.ErrMsg);
      else{
        $("#inputST").val(data.Setting.Starttime)
        $("#inputET").val(data.Setting.Endtime)
        $("#inputCnt").val(data.Setting.Cnt)
        var h1=data.Setting.Starttime.split(":")
        var h2=data.Setting.Endtime.split(":")
        $("#inputH1").val(h1[0])
        $("#inputM1").val(h1[1])
        $("#inputH2").val(h2[0])
        $("#inputM2").val(h2[1])
      }
    }else{
      mDialog.Check(data.ErrMsg);
    }
  });
  $('.save-block').click(function () {
    if ($("#inputH1").val()>23||$("#inputH2").val()>23){
      mDialog.Check("小時輸入錯誤");
    }
    else  if ($("#inputM1").val()>59||$("#inputM2").val()>59){
      mDialog.Check("分鐘輸入錯誤");
    }
    else  if ($("#inputH1").val()<0||$("#inputH2").val()<0){
      mDialog.Check("小時輸入錯誤");
    }
    else  if ($("#inputM1").val()<0||$("#inputM2").val()<0){
      mDialog.Check("分鐘輸入錯誤");
    }
    else {
      $("#inputST").val( $("#inputH1").val()+":"+ $("#inputM1").val())
      $("#inputET").val( $("#inputH2").val()+":"+ $("#inputM2").val())
      WebAPI.setSetting(mUser.Token,$("#inputST").val(),$("#inputET").val(),$("#inputCnt").val(),  function (data) {
        if (data.ErrCode == 0 || data.ErrCode == 103){
          if (data.ErrCode == 103)
            mDialog.Check(data.ErrMsg);
          else
            mDialog.Check("儲存成功");
        }else{
          mDialog.Check(data.ErrMsg);
        }
      });
    }
  });

  $("#inputH1,#inputH2").keydown(function (e) {
    // console.log(String.fromCharCode((96 <= e.keyCode && e.keyCode <= 105) ? e.keyCode-48 : e.keyCode) )
    var v=String.fromCharCode((96 <= e.keyCode && e.keyCode <= 105) ? e.keyCode-48 : e.keyCode);
    var val= parseInt($(this).val()+v);
    console.log(val)
    if (val>23){return false}
  })

  $("#inputM1,#inputM2").keydown(function (e) {
    var v=String.fromCharCode((96 <= e.keyCode && e.keyCode <= 105) ? e.keyCode-48 : e.keyCode);
    var val= parseInt($(this).val()+v);
    console.log(val)
    if (val>60){return false}
  })

});