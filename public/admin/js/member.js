var mMember = {
    state: '',
    search: '',
    ids: [],
    getList: function (callback) {
        // console.error('mRecord.getMemberlist.start(): ', new Date());
        WebAPI.getMemberlist(mUser.Token, this.state, this.search, function (data) {
            // console.error('mRecord.getMemberlist.end(): ', new Date());
           if (data.ErrCode == 0 || data.ErrCode == 103){
               if (data.ErrCode == 103) mDialog.Check(data.ErrMsg);
               mMember.ids = []; //Clear and reload.
               callback(data);
           }else{
               mDialog.Check(data.ErrMsg);
           }
        });
    },
    getListItem: function (data) {
        this.ids.push({
            id: data.Memberid,
            phone: data.Phone
        });
        var checkBox = '<input id="_select_'+data.Memberid+'" type="checkbox" class="hide" name="selectBox"><label for="_select_'+data.Memberid+'" class="select-check"></label>';
        var isGeneral = data.State == 'Y' ? 'selected' : '';
        var isBlack = data.State == 'N' ? 'selected' : '';
        var switchBtn = '<div role="state" class="switch-block"><div role="Y" class="btn-switch '+isGeneral+'">一般</div><div role="N" class="btn-switch '+isBlack+'">黑名單</div></div>';
        return [checkBox, data.Phone, data.Sum1, data.Sum2, data.Remark,data.Mremark, switchBtn];
    },
    setSearchOpts: function () {
        var $select = $('#searchType');
        mMember.search = $('#searchInput').val();
        if ($select.is(':disabled')){
            mMember.state = '';
        }else{
            mMember.state = $select.val() ? $select.val() : '';
        }
    },
    setMbState: function (id, state) {
        WebAPI.setMbrstate(mUser.Token, id, state, function (data) {
            if (data.ErrCode == 0){

            }else{
                mDialog.Check(data.ErrMsg);
            }
        })
    },
    deleteMb: function (id, callback) {
        mLoading.start();
        WebAPI.deleteMember(mUser.Token, id, function (data) {
            if (data.ErrCode == 0){
                callback();
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    init: function ($db) {
        this.getList(function (data) {
            var separatedList = mHandle.separatedData.getData(data.Memberlist);
            mHandle.separatedData.setLayout(separatedList, $db);

            // var list = [];
            // var dataList = data.Memberlist;
            // if (dataList){
            //     if (dataList.length > 0) {
            //         $.each(data.Memberlist, function (i, item) {
            //             list.push(mMember.getListItem(item));
            //         });
            //     }
            // }
            //
            // $db
            //     .clear()
            //     .rows
            //     .add(list)
            //     .draw();
        });
    }
};

$(function () {
    var $db = $('#dbMember')
        .DataTable({
            lengthChange: dataDB.isTbChange,
            iDisplayLength: dataDB.tbLength,
            ordering: false,
            // order: [[0, 'desc']],
            // columnDefs: [{
            //     targets: [0,1,2,3,4],
            //     orderable: false
            // }],
            searching: dataDB.isSearch,
            info: dataDB.isInfo,
            dom: '<"db_row dbExport"<"dbToolbar3">Bf><"db_row"rt><"db_row" ip>',
            buttons:[{
                extend: 'excel',
                className: 'btn-sm',
                title: 'Call List - Blacklist'
            }],
            createdRow: function (row, data, dataIndex) {
                if (mMember.ids.length > 0){
                    $(row).attr({
                        'data-id': mMember.ids[dataIndex].id,
                        'data-phone': mMember.ids[dataIndex].phone
                    });
                    // console.log(" mMember.ids[dataIndex]: ",  mMember.ids[dataIndex]);
                    // console.log("row: ", row);
                    // console.log("data: ", data);
                    // console.log("dataIndex: ", dataIndex);
                }
            }
        })
        .on('click', 'tbody td', function (e) {
            // console.log('e.target: ', e.target);
            // console.log('this: ', this);
            // console.log('isSelect: ', isSelect);
            var isSelect = $(this).find('label.select-check').length > 0;
            var isSwitch = $(this).find('.switch-block').length > 0;
            var isEmpty = $(this).is('.dataTables_empty');
            if (!isSelect && !isSwitch && !isEmpty) {
                var id = $(this).parent('tr').data('id');
                var phone = $(this).parent('tr').data('phone');
                window.location.href = 'member_content?mbid='+id+'&phone='+phone;
            }
        })
        .on('click', '.btn-switch', function () {
            var $btnS = $(this);
            var $switchBtn = $btnS.parent();
            var $switchs = $switchBtn.find('.btn-switch');
            $switchs.each(function (i, btn) {
                $(btn).removeClass('selected');
            });
            $btnS.addClass('selected');
            mMember.setMbState($btnS.parents('tr').data('id'), $btnS.attr('role'));
        });

    // mMember.init($db);

    $('#searchBtn').click(function () {
        mMember.setSearchOpts();
        mMember.init($db);
    });
    $('#searchInput').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });

    $('.delete-block').click(function () {
        var $checked = $('input[name="selectBox"]:checked');
        var checkTotal = $checked.length;
        var checkCount = 0;
        if (checkTotal > 0){
            mDialog.Confirm("確定刪除會員?", "", function (dialog) {
                dialog.close();
                $checked.each(function (i, item) {
                    var id = $(item).attr('id').replace('_select_', '');
                    // console.log('id: ', id);
                    mLoading.start();
                    mMember.deleteMb(id, function () {
                        checkCount++;
                        if (checkCount == checkTotal){
                            var isComplete = mLoading.complete();
                            if (isComplete){
                                mMember.init($db);
                            }
                        }
                    });
                });
            })
        }else{
            mDialog.Check("請先勾選欲刪除的項目");
        }
    });

    $('#searchType').change(function () {
       this.value == 'N' ? $('#exportExcel').removeClass('hidden') : $('#exportExcel').addClass('hidden');
    });

    $('#exportExcel').click(function () {
        $('.dbExport').find('.buttons-excel').trigger('click');
    });

});