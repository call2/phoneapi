var mBusinessOpts = {
    getList: function (callback) {
        WebAPI.getSaleslist(mUser.Token, '', function (data) {
            if (data.ErrCode == 0){
                callback(data.Sales);
            }
        });
    },
    setOptList: function () {
        this.getList(function (data) {
            $.each(data, function (i, item) {
                $('#searchType').append('<option value="'+item.Salesid+'">'+item.Salesname+'</option>');
            });
        });
    },
    init: function () {
        this.setOptList();
    }
};

var mStartDate = {
    getOptList: function () {
        var date = new Date();
        var thisM = date.getFullYear()+'/'+(date.getMonth()+1);
        var lastY = date.getMonth() <= 0 ? (date.getFullYear()-1):date.getFullYear();
        var lastM = date.getMonth() <= 0 ? '12':date.getMonth();
        var lastDate = lastY + '/' + lastM;
        var sixY = date.getMonth() <= 5 ? (date.getFullYear()-1):date.getFullYear();
        var sixM = date.getMonth() <= 5 ? 12+date.getMonth()-5+1:date.getMonth()-5+1;
        if (sixM>12) sixM-=1;
        var sixDate = sixY + '/' + sixM;
        return '<option value="'+lastDate+'">前月</option><option value="'+thisM+'">當月</option><option value="6months" selected="selected">半年內</option><option value="3days">近三日</option>';
      // return '<option selected disabled>月份</option><option value="'+lastDate+'">前月</option><option value="'+thisM+'">當月</option><option value="'+sixDate+'">半年內</option><option value="3days" selected="selected">近三日</option>';
    },
    init: function () {
        $('#searchDate').html(this.getOptList());
    }
};

var mRecord = {
    startDate: '',
    isMb: '',
    saleID: '',
    search: '',
    getList: function (callback) {
        WebAPI.getPhonerecord(mUser.Token, '', this.startDate, this.isMb, this.saleID, this.search, function (data) {
            if (data.ErrCode == 0){
                callback(data);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    getListItem: function (data) {
        var mstate=(data.Memberstate=="N")?"-黑名單":"";
        return [data.Datestr, data.Phone, data.Ismember == 'Y' ? '會員'+mstate:'非會員', data.Salesname, data.Inout];
    },
    setSearchOpts: function () {
        var info = this;
        var $select = $('#searchType'),
            $searchDate = $('#searchDate'),
            $searchStatus = $('#searchStatus');
        info.search = $('#searchInput').val();
        if ($select.is(':disabled')){
            info.saleID = '';
        }else{
            info.saleID = $select.val() ? parseInt($select.val()) : '';
        }

        if ($searchDate.is(':disabled')){
            info.startDate = '';
        }else{
            info.startDate = $searchDate.val() ? $searchDate.val() : '';
        }

        if ($searchStatus.is(':disabled')){
            info.isMb = '';
        }else{
            info.isMb = $searchStatus.val() ? $searchStatus.val() : '';
        }
        console.log(info)
    },
    init: function ($db) {
        this.getList(function (data) {
            var separatedList = mHandle.separatedData.getData(data.Records);

            //Test
            // var separatedList = mHandle.separatedData.getData(testJSON.getData());
            // $('#costTotal').text(testJSON.getData().length);

            mHandle.separatedData.setLayout(separatedList, $db);
        });
    }
};

$(function () {
    mBusinessOpts.init();
    mStartDate.init();
    var $db = $('#dbCall').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        ordering: false,
        searching: dataDB.isSearch,
        info: dataDB.isInfo
    });

    // mRecord.init($db);

    $('#searchBtn').click(function () {
        mRecord.setSearchOpts();
        // console.log("mRecord: ", mRecord);
        mRecord.init($db);
    });
    $('#searchInput').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });
});