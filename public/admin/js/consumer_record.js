var mBusinessOpts = {
    getList: function (callback) {
        WebAPI.getSaleslist(mUser.Token, '', function (data) {
            if (data.ErrCode == 0){
                callback(data.Sales);
            }
        });
    },
    setOptList: function () {
        this.getList(function (data) {
            $.each(data, function (i, item) {
                $('#searchType').append('<option value="'+item.Salesid+'">'+item.Salesname+'</option>');
            });
        });
    },
    init: function () {
        this.setOptList();
    }
};

var mRecord = {
    startDate: '',
    endDate: '',
    saleID: '',
    saleName: '',
    search: '',
    db: '',
    getList: function (callback) {
        // console.error('mRecord.getList.start(): ', new Date());
        WebAPI.getSalesrecord(mUser.Token, '', this.startDate, this.endDate, this.saleName, this.search, function (data) {
            // console.error('mRecord.getList.end(): ', new Date());
            if (data.ErrCode == 0 || data.ErrCode == 103){
                if (data.ErrCode == 103) mDialog.Check(data.ErrMsg);
                callback(data);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    getListItem: function (data) {
        var checkBox = '<input id="_select_'+data.id+'" type="checkbox" class="hide" name="selectBox"><label for="_select_'+data.id+'" class="select-check"></label>';
        return [checkBox, data.Datestr, data.Phone, data.Salesname, data.Remark];
    },
    setSearchOpts: function () {
        var info = this;
        var $select = $('#searchBusiness'),
            // $select = $('#searchType'),
            $dateStart = $('#searchDateS'),
            $dateEnd = $('#searchDateE');
        info.search = $('#searchInput').val();
        if ($select.is(':disabled')){
            // info.saleID = '';
            info.saleName = '';
        }else{
            // info.saleID = $select.val() ? parseInt($select.val()) : '';
            info.saleName = $select.val() ? $select.val() : '';
        }

        if ($dateStart.is(':disabled') || $dateEnd.is(':disabled')){
            info.startDate = '';
            info.endDate = '';
        }else{
            info.startDate = $dateStart.val();
            info.endDate = $dateEnd.val();
        }
    },
    init: function ($db) {
        this.db = $db;
        this.getList(function (data) {
            var separatedList = mHandle.separatedData.getData(data.Records);

            //Test
            // var separatedList = mHandle.separatedData.getData(testJSON.getData());
            // $('#costTotal').text(testJSON.getData().length);

            mHandle.separatedData.setLayout(separatedList, $db);
        });
    },
    uploadExcel: function (obj, callback) {
        // console.log("obj: ", obj);
        // console.error('mRecord.uploadExcel.start(): ', new Date());
        WebAPI.importSalesrecord(mUser.Token, obj, function (data) {
            // console.error('mRecord.uploadExcel.end(): ', new Date());
            if (data.ErrCode == 0){
                callback('success');
            }else if (data == 'ajaxError'){
                callback(data);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    },
    separatedUpload: function (dataArray) {
        //To separate the data, and separate to upload the data.
        var num = 0;
        if (num == 0) {
            mHandle.progress.init(dataArray.length);
            mLoading.setCustomAlert('匯入中(#0#%)', 'uploading');
            mLoading.start('uploading');
        }
        var response = function (errMsg) {
            if (errMsg == 'success'){
                mHandle.progress.addCount();
                num++;
                if (num >= dataArray.length){
                    mLoading.complete();
                    mDialog.Check('匯入成功');
                    if (mRecord.db) mRecord.init(mRecord.db);
                } else{
                    mRecord.uploadExcel(dataArray[num], response);
                }
            } else{
                mRecord.uploadExcel(dataArray[num], response);
            }
        };
        mRecord.uploadExcel(dataArray[num], response);
    },
    deleteRecord: function (records, callback) {
        WebAPI.deleteSalesrecord(mUser.Token, records, function (data) {
            if (data.ErrCode == 0){
                mDialog.Check("刪除成功");
                callback();
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    }
};

/*Parse import file 'excel'*/
var rABS = true;    //true: readAsBinaryString; false: readAsArrayBuffer
function toJSON(workbook) {
    var result = [];
    workbook.SheetNames.forEach(function (sheetName) {
        // console.log('workbook.Sheets[sheetName]: ', workbook.Sheets[sheetName]);
        var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header: ["YYYYMMDD", "Salesname", "Phone", "Remark"], dateNF: "YYYY/MM/DD"});
        if (roa.length > 0) {
            roa.shift();
            result[sheetName] = roa;
        }
    });
    return result;
}
function handleFile(e) {
    var files = e.target.files, f = files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        var data = e.target.result;
        if (!rABS) data = new Uint8Array(data);
        var workbook = XLSX.read(data, {type: rABS ? 'binary' : 'array', cellDates: true, cellNF: false, cellText: false});

        // console.log('workbook', workbook);

        var dataObj = toJSON(workbook);
        var dataKeys = Object.keys(dataObj);
        if (dataKeys.length > 0){
            $.each(dataKeys, function (i, key) {
                // console.log(dataObj[key]);
                var separateRange = 100;
                var separatedData = mHandle.separatedData.getDataRange(dataObj[key], separateRange);
                mRecord.separatedUpload(separatedData);
            });
        }
        $("#importInput").val("");
    };
    if (rABS) reader.readAsBinaryString(f); else reader.readAsArrayBuffer(f);
}
/*-----------------------*/

var exportFile1 = "消費紀錄報表";

$(function () {
    // mBusinessOpts.init();

    var $db = $('#dbConsumer').DataTable({
        lengthChange: dataDB.isTbChange,
        iDisplayLength: dataDB.tbLength,
        order: [[1, 'desc']],
        columnDefs: [{
            targets: [0,1,2,3,4],
            orderable: false
        }],
      searching: true,
      language: {
        search: "<label style='font-size: 12px;font-weight: bold;color: black'>關鍵字查詢</label>:"
      },
        info: dataDB.isInfo,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                className: 'btn-excel',
                title: exportFile1
            }
        ]
    });

    mRecord.init($db);

    $('#searchBtn').click(function () {
        mRecord.setSearchOpts();
        // console.log("mRecord: ", mRecord);
        mRecord.init($db);
    });
    $('#searchInput').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });
    $('#searchBusiness').keypress(function (e) {
        if (e.keyCode == 13){
            $('#searchBtn').trigger('click');
        }
    });

    $('#importInput').change(handleFile);
    $('#btnImport').click(function () {
        $('#importInput').trigger('click');
    });

    $('#btnExport').click(function () {
        $('.btn-excel').trigger('click');
    });

    $('.delete-block').click(function () {
        var $checked = $('input[name="selectBox"]:checked');
        var checkTotal = $checked.length;
        // var checkCount = 0;
        if (checkTotal > 0){
            mDialog.Confirm("確定刪除消費紀錄?", "", function (dialog) {
                dialog.close();
                var records = [];
                mLoading.start();
                $checked.each(function (i, item) {
                    var id = $(item).attr('id').replace('_select_', '');
                    // console.log('id: ', id);
                    records.push(parseInt(id));
                });
                mRecord.deleteRecord(records, function () {
                    mRecord.init($db);
                    mLoading.complete();
                });
            })
        }else{
            mDialog.Check("請先勾選欲刪除的項目");
        }
    });

    // mHandle.getThisLastDate();
});