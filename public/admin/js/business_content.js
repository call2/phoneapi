var mBusinessDetail = {
    info: JSON.parse(sessionStorage['saleInfo']),
    init: function () {
      tools.zipList().forEach(function (val) {
        $("#sltCity").append($('<option>',{value: val.name,text : val.name}));
      })

      var info = this.info;
        $('#uuidTitle').text(info.uuid);
        $('#inputTitle').val(info.saleName);
        $('#inputZip').val(info.Zip);
        // setArea(info.Zip);// setArea("304");
        $('.switch-block').each(function (i, item) {
            var role = $(item).attr('role');
            switch (role){
                case "cost":
                    info.isCost == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "black":
                    info.isBlack == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "remark":
                    info.isRemark == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
                case "report":
                    info.isReport == 'Y' ? $(item).children('[role="open"]').addClass('selected').next('[role="close"]').removeClass('selected') : '';
                    break;
            }
        });
    },
    setStatus: function (type, status) {
        var isOpen = status == 'open' ? 'Y' : 'N';
        switch (type){
            case "cost":
                this.info.isCost = isOpen;
                break;
            case "black":
                this.info.isBlack = isOpen;
                break;
            case "remark":
                this.info.isRemark = isOpen;
                break;
            case "report":
                this.info.isReport = isOpen;
                break;
        }
    },
    saveInfo: function () {
        // console.log(mBusinessDetail.info);
        WebAPI.setSalesinfo(mUser.Token, this.info.id, this.info.saleName, this.info.isCost, this.info.isBlack, this.info.isRemark, this.info.isReport,$('#inputZip').val(), function (data) {
            if (data.ErrCode == 0){
                mDialog.Check("儲存成功");
                setTimeout(function () {
                    window.location.href = "business";
                }, 1500);
            }else{
                mDialog.Check(data.ErrMsg);
            }
        });
    }
};

function setArea(zip) {
  var city="臺北市";var area;var ziplist=tools.zipList();
  if (zip!=""){
      for (var i=0;i< ziplist.length;++i){
        area= ziplist[i].districts.filter(function (dd) {
          return dd.zip==zip;
        })
        if (area.length>0){
          city=ziplist[i].name;
          break;
        }
      }
  }
  setArealist(city);
  $("#sltCity").val(city);
  if (zip!=""){
    $("#sltArea").val(zip);
  }
}
function setArealist(city) {
  var areas=tools.zipList().filter(function (val) {
    return val.name==city;
  })
  if (areas.length>0){
    $('#sltArea')[0].options.length = 0;
    $("#sltArea").append($('<option>',{value: "0",text : "區域"}));
    areas[0].districts.forEach(function (val) {
      $("#sltArea").append($('<option>',{value: val.zip,text : val.name}));
    })
  }
}
$(function () {
    mBusinessDetail.init();

    $("#sltCity").on("change",function () {
      // console.log($(this).val())
      setArealist($(this).val())
    })

    $('.switch-block').on('click', '.btn-switch', function () {
        var $btnS = $(this);
        var $switchBtn = $btnS.parent();
        var $switchs = $switchBtn.find('.btn-switch');
        $switchs.each(function (i, btn) {
            $(btn).removeClass('selected');
        });
        $btnS.addClass('selected');
        mBusinessDetail.setStatus($switchBtn.attr('role'), $btnS.attr('role'));
    });

    $('.save-block').click(function () {
      mBusinessDetail.saveInfo();
        // if ($("#sltArea").val()=="0"){
        //   mDialog.Check("請選擇區域");
        // }
        // else {
        //   mBusinessDetail.saveInfo();
        // }
    });

    $('#inputTitle').change(function () {
        mBusinessDetail.info.saleName = this.value;
    });

});