define({ "api": [
  {
    "type": "post",
    "url": "/adminmgr/deleteMember",
    "title": "010408.deleteMember",
    "name": "deleteMember",
    "group": "Admin",
    "description": "<p>會員管理/ 刪除會員</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/deleteMember"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/deleteSales",
    "title": "010305.deleteSales",
    "name": "deleteSales",
    "group": "Admin",
    "description": "<p>業務人員管理 / 刪除業務人員</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Salesid",
            "description": "<p>業務人員ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>密碼錯誤</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/deleteSales"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/deleteSalesrecord",
    "title": "010705.deleteSalesrecord",
    "name": "deleteSalesrecord",
    "group": "Admin",
    "description": "<p>消費紀錄管理/  消費紀錄刪除</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int[]",
            "optional": false,
            "field": "Arr",
            "description": "<p>ID陣列</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/deleteSalesrecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getCrashlog",
    "title": "0110.getCrashlog",
    "name": "getCrashlog",
    "group": "Admin",
    "description": "<p>消費紀錄管理/  消費紀錄查詢</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records",
            "description": "<p>消費紀錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Datestr",
            "description": "<p>年月日</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Logtxt",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getCrashlog"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getMbrrecord",
    "title": "0106.getMbrrecord",
    "name": "getMbrrecord",
    "group": "Admin",
    "description": "<p>會員管理/  會員消費紀錄查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Startym",
            "description": "<p>起始年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Endym",
            "description": "<p>結束年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Salesname",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢備註關鍵字</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Mbrremark",
            "description": "<p>會員備註</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records",
            "description": "<p>消費紀錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Datestr",
            "description": "<p>年月日</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Salesname",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Remark",
            "description": "<p>消費備註</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getMbrrecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getMemberlist",
    "title": "0104.getMemberlist",
    "name": "getMemberlist",
    "group": "Admin",
    "description": "<p>會員管理/ 查詢會員列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "State",
            "description": "<p>查詢身分 &quot;&quot;:全部  Y:一般會員  N:黑名單</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢電話</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Memberlist",
            "optional": false,
            "field": "Memberlist",
            "description": "<p>會員清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Memberlist.Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Memberlist.Sum1",
            "description": "<p>上一個月消費次數</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Memberlist.Sum2",
            "description": "<p>總消費次數</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Memberlist.State",
            "description": "<p>身份狀態</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getMemberlist"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getPerformance",
    "title": "0113.getPerformance",
    "name": "getPerformance",
    "group": "Admin",
    "description": "<p>業務員績效查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Startym",
            "description": "<p>起始年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Endym",
            "description": "<p>結束年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢電話號碼</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records",
            "description": "<p>業務員績效紀錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.YYYYMM",
            "description": "<p>年月</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Salesname",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Records.Cnt",
            "description": "<p>次數</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getPerformance"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getPhonerecord",
    "title": "0109.getPhonerecord",
    "name": "getPhonerecord",
    "group": "Admin",
    "description": "<p>來電記錄/  來電記錄查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Startym",
            "description": "<p>起始年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Ismember",
            "description": "<p>身份 &quot;&quot;:全部 Y:會員 N:非會員</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Salesid",
            "description": "<p>業務員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢電話號碼</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records",
            "description": "<p>消費紀錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Datestr",
            "description": "<p>年月</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Phone",
            "description": "<p>會員電話</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Ismember",
            "description": "<p>身分   Y:會員 N:非會員</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Salesname",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Inout",
            "description": "<p>'':來電  '撥出':撥出'</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getPhonerecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getSaleslist",
    "title": "0102.getSaleslist",
    "name": "getSaleslist",
    "group": "Admin",
    "description": "<p>業務人員管理/ 查詢業務人員列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢關鍵字</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Sales",
            "optional": false,
            "field": "Sales",
            "description": "<p>業務人員清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Salesid",
            "description": "<p>業務人員ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.UUID",
            "description": "<p>業務人員UUID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Salesname",
            "description": "<p>業務人員名稱</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Show1",
            "description": "<p>是否顯示消費紀錄</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Show2",
            "description": "<p>是否顯示黑名單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Show3",
            "description": "<p>是否顯示會員備註</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Sales.Show4",
            "description": "<p>是否顯示回報</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getSaleslist"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getSalesrecord",
    "title": "0107.getSalesrecord",
    "name": "getSalesrecord",
    "group": "Admin",
    "description": "<p>消費紀錄管理/  消費紀錄查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Startym",
            "description": "<p>起始年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Endym",
            "description": "<p>結束年月(YYYY/MM)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Salename",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Qrystr",
            "description": "<p>查詢備註關鍵字</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Cnt",
            "description": "<p>總筆數</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records",
            "description": "<p>消費紀錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Records.id",
            "description": "<p>消費紀錄ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Datestr",
            "description": "<p>年月日</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Phone",
            "description": "<p>會員電話</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Salesname",
            "description": "<p>業務員</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Records.Remark",
            "description": "<p>消費備註</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getSalesrecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/getSetting",
    "title": "0111.getSetting",
    "name": "getSetting",
    "group": "Admin",
    "description": "<p>時間設定/  時間設定查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/getSetting"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/importSalesrecord",
    "title": "0108.importSalesrecord",
    "name": "importSalesrecord",
    "group": "Admin",
    "description": "<p>消費紀錄管理/  匯入消費紀錄</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "JSON",
            "optional": false,
            "field": "Data[]",
            "description": "<p>匯入資料JSON陣列 ex.:[{&quot;YYYYMMDD&quot;:&quot;2017/11/01&quot;,&quot;Salesname&quot;:&quot;test1&quot;,&quot;Phone&quot;:&quot;0911123123&quot;,&quot;Remark&quot;:&quot;22212&quot;},{&quot;YYYYMMDD&quot;:&quot;2017/11/02&quot;,&quot;Salesname&quot;:&quot;test1&quot;,&quot;Phone&quot;:&quot;0911123123&quot;,&quot;Remark&quot;:&quot;22212&quot;},...]</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/importSalesrecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/loginAdmin",
    "title": "0101.loginAdmin",
    "name": "loginAdmin",
    "group": "Admin",
    "description": "<p>管理員登入</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Password",
            "description": "<p>密碼</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>密碼錯誤</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/loginAdmin"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/setMbrremark",
    "title": "0105.setMbrremark",
    "name": "setMbrremark",
    "group": "Admin",
    "description": "<p>會員管理/  編輯會員備註</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Remark",
            "description": "<p>備註</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/setMbrremark"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/setMbrstate",
    "title": "010405.setMbrstate",
    "name": "setMbrstate",
    "group": "Admin",
    "description": "<p>會員管理/  設定會員黑名單(恢復)</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Memberid",
            "description": "<p>會員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "State",
            "description": "<p>Y:一般會員 N:黑名單</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/setMbrstate"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/setSalesinfo",
    "title": "0103.setSalesinfo",
    "name": "setSalesinfo",
    "group": "Admin",
    "description": "<p>業務人員管理 / 業務人員權限設定</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "Int",
            "optional": false,
            "field": "Salesid",
            "description": "<p>業務人員ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Salesname",
            "description": "<p>業務人員名稱</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Show1",
            "description": "<p>是否顯示消費紀錄</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Show2",
            "description": "<p>是否顯示黑名單</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Show3",
            "description": "<p>是否顯示會員備註</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Show4",
            "description": "<p>是否顯示回報</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Zip",
            "description": "<p>郵遞區號</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401",
            "description": "<p>密碼錯誤</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/setSalesinfo"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/adminmgr/setSetting",
    "title": "0112.setSetting",
    "name": "setSetting",
    "group": "Admin",
    "description": "<p>setSetting/  setSetting</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>Token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Starttime",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Endtime",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "Cnt",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "100",
            "description": "<p>此Token無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "103",
            "description": "<p>查無此資料</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/adminmgr/setSetting"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/back/adminmgr.js",
    "groupTitle": "Admin"
  },
  {
    "type": "post",
    "url": "/member/qryPhonerecordCR1",
    "title": "0107.Member_qryPhonerecordCR1",
    "name": "Member_qryPhonerecordCR1",
    "group": "CR1_Front",
    "description": "<p>查詢來電的客戶紀錄CR1</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>來電電話(只需傳入後9碼,不含 &quot;-&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phonestr",
            "description": "<p>完整來電電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Version",
            "description": "<p>Version</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "isadd",
            "description": "<p>(來電:true  查詢:false)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Count",
            "description": "<p>消費次數代號(0：消費0次  1：消費1次  1+：消費2~10次  2+：消費11~99次  3+：消費100次以上)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Blacklist",
            "description": "<p>是否為黑名單 (OK NO 0)</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/qryPhonerecordCR1"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "CR1_Front"
  },
  {
    "type": "post",
    "url": "/member/addCalloutrecord",
    "title": "010205.Member_addCalloutrecord",
    "name": "Member_addCalloutrecord",
    "group": "Front",
    "description": "<p>新增撥出電話紀錄</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>撥出電話(只需傳入後9碼,不含 &quot;-&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phonestr",
            "description": "<p>完整來電電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Version",
            "description": "<p>Version</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/addCalloutrecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/qryDailyreport",
    "title": "0104.Member_qryDailyreport",
    "name": "Member_qryDailyreport",
    "group": "Front",
    "description": "<p>查詢當日回報列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Salesname",
            "description": "<p>業務專員</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Show4",
            "description": "<p>是否顯示回報</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records[]",
            "description": "<p>消費記錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Datestr",
            "description": "<p>消費年月</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>客戶電話</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Remark",
            "description": "<p>消費備註</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/qryDailyreport"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/qryMember",
    "title": "0103.Member_qryMember",
    "name": "Member_qryMember",
    "group": "Front",
    "description": "<p>會員查詢</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>來電電話(只需傳入後9碼,不含 &quot;-&quot;)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Sum1",
            "description": "<p>上一個月消費次數</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Sum2",
            "description": "<p>消費總次數</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Show1",
            "description": "<p>是否顯示消費紀錄</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Show2",
            "description": "<p>業務員是否可看黑名單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "State",
            "description": "<p>N:黑名單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Remark",
            "description": "<p>後台備註</p>"
          },
          {
            "group": "Success 200",
            "type": "Records",
            "optional": false,
            "field": "Records[]",
            "description": "<p>消費記錄清單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Datestr",
            "description": "<p>消費年月</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Salesname",
            "description": "<p>業務專員</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/qryMember"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/qryPhonerecord",
    "title": "0102.Member_qryPhonerecord",
    "name": "Member_qryPhonerecord",
    "group": "Front",
    "description": "<p>查詢來電的客戶紀錄</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>來電電話(只需傳入後9碼,不含 &quot;-&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phonestr",
            "description": "<p>完整來電電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Version",
            "description": "<p>Version</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "Count",
            "description": "<p>消費總次數</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Blacklist",
            "description": "<p>是否為黑名單</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Remark",
            "description": "<p>是否顯示會員備註</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/qryPhonerecord"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/saleReport",
    "title": "0105.Member_saleReport",
    "name": "Member_saleReport",
    "group": "Front",
    "description": "<p>將消費紀錄回傳至後台</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone",
            "description": "<p>客戶電話</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Remark",
            "description": "<p>消費備註</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Show4",
            "description": "<p>是否顯示回報</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/saleReport"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/sendLog",
    "title": "0106.Member_sendLog",
    "name": "Member_sendLog",
    "group": "Front",
    "description": "<p>錯誤紀錄回傳至後台</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Logtxt",
            "description": "<p>Exceptionlog</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/sendLog"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  },
  {
    "type": "post",
    "url": "/member/sendUUID",
    "title": "0101.Member_sendUUID",
    "name": "Member_sendUUID",
    "group": "Front",
    "description": "<p>會員登入</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "UUID",
            "description": "<p>UUID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ErrCode",
            "description": "<p>錯誤代碼</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ErrMsg",
            "description": "<p>錯誤訊息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "0",
            "description": "<p>成功</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "999",
            "description": "<p>系統維護中</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "102",
            "description": "<p>此UUID無效</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "250",
            "description": "<p>輸入參數不可空白</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/member/sendUUID"
      }
    ],
    "version": "0.0.0",
    "filename": "routes/front/member.js",
    "groupTitle": "Front"
  }
] });
