/**
 * Created by ralph on 8/7/2017.
 */
var models  = require('../models');

// mongoose schema - OTP
// var OTP = require('../models/otp');
// moment: date/time format
var moment = require('moment-timezone');
var validator = require('validator');

function tool() {

}
tool.Admintype={"0":"最高管理員","1":"一般管理員"};
tool.TransTypeTxt={"A":"儲值金充值", "B":"司機接案入帳", "C":"司機優惠出帳", "D":"公司分潤出帳", "E":"保險費出帳", "F":"公司分潤入帳", "G":"公司保險費入帳",
    "H":"優惠差額入帳","I":"優惠差額出帳","J":"儲值金扣款"};
//"A":"儲值金充值"(D), "B":"司機接案入帳"(D+), "C":"司機優惠出帳"(D-), "D":"公司分潤出帳"(D-), "E":"保險費出帳"(D-), "F":"公司分潤入帳"(C+), "G":"公司保險費入帳"(C+),
//"H":"優惠差額入帳"(D+),"I":"優惠差額出帳"(C-),"J":"儲值金扣款"(D-)

tool.YMDHMS="YYYY/MM/DD HH:mm:ss";
tool.YMDHM="YYYY/MM/DD HH:mm";
tool.YMD="YYYY/MM/DD";
tool.YM="YYYY/MM";
tool.MD="MM/DD";
tool.NOF="YYYYMMDD";
tool.Insurance=80; //待客戶確認

tool.chkSameZip=function (docs,cnt) {
  var arr=[]
  docs.forEach(function (val) {
    if (val.Sale.Zip!="" && arr.indexOf(val.Sale.Zip)<0){
        arr.push(val.Sale.Zip);
    }
  })
  return arr.length<cnt;
}
tool.chkPeriod=function(now,Starttime,Endtime){
  var ymd=new moment(now,"YYYYMMDDHHmmss").tz("Asia/Taipei").format("YYYY-MM-DD");
  var st=new moment(ymd+" "+Starttime+":00").format("YYYYMMDDHHmmss");
  var et=new moment(ymd+" "+Endtime+":59").format("YYYYMMDDHHmmss");
  if (now<st) st=new moment(ymd+" "+Starttime+":00").add(-1,'days').format("YYYYMMDDHHmmss");
  if (st>et) et=new moment(ymd+" "+Endtime+":59").add(1,'days').format("YYYYMMDDHHmmss");
  // console.log("*******now st et*****  "+now+"  "+st+"   "+et);
  return (st<=now && now<et);
}
tool.getSetting=function (callback) {
  models.Setting.findOne({}).then(function (data) {
    callback(data);
  })
}
tool.chkSale=function (user) {
  return user.Show1=="Y" || user.Show2=="Y" || user.Show3=="Y" || user.Show4=="Y"
}
tool.getSNO=function (type,callback) {
    var nof=type+moment().format(tool.NOF);
    models.Caserecord.count({where:{Caseid:{$like:nof+'%'}}}).then(function (cnt) {
        var no=cnt+1;
        callback(nof+padDigits(no,4));
    })
}
//檢查當天是否有未完成的案件
tool.chkNotComplete=function (memberid,callback) {
    var startdate=moment(moment().tz("Asia/Taipei").format("YYYY-MM-DD"),"YYYY-MM-DD");
    var enddate=moment(moment().tz("Asia/Taipei").format("YYYY-MM-DD"),"YYYY-MM-DD").add(1,"Days");
    models.Caserecord.count({where:{$and:[{MemberId:memberid},{State:null},{createdAt:{$gte:startdate}},{createdAt:{$lt:enddate}}]}}).then(function (cnt) {
        callback(cnt>0);
    })
}
//檢查1分鐘之內是否有銷售紀錄
tool.chkSalesrecord=function (phone,saleid,callback) {
    // var startdate=moment(moment().tz("Asia/Taipei").format("YYYY-MM-DD"),"YYYY-MM-DD");
    var enddate=moment().add(-1,"Minutes");
    models.Salesrecord.count({where:{$and:[{Phone:phone},{SaleId:saleid},{Cmsdate:{$gte:enddate}}]}}).then(function (cnt) {
        callback(cnt>0);
    })
}
function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}
tool.isOffer=function (mbr,callback) {
    if (mbr.Birthday!=null && mbr.Birthday!="" && mbr.Birthday==moment().tz("Asia/Taipei").format("MM/DD")){
        //檢查是否本月第一次消費
        var startdate=moment(moment().tz("Asia/Taipei").format("YYYY-MM")+"-01","YYYY-MM-DD");
        var enddate=moment(moment().tz("Asia/Taipei").format("YYYY-MM")+"-01","YYYY-MM-DD").add(1,"Months");
        models.Transactionlog.count({where:{$and:[{createdAt:{$gte:startdate}},{createdAt:{$lt:enddate}}]}}).then(function (cnt) {
            callback((cnt==0)?"Y":"N");
        })
    }
    else
        callback("N");
}
tool.getFeeList=function (callback) {
    var arr=[];
    models.Feesetup.findOne({order:[["createdAt","DESC"]]}).then(function (doc) {
        if (doc!=null){
            arr.push(doc.Price1);
            arr.push(doc.Price2);
            arr.push(doc.Price3);
        }
        callback(arr);
    })
}

tool.arrCatstr=function (arr,sep) {
    var res="";
    for (var i=0;i<arr.length;++i){
        res+=arr[i].toString()+sep;
    }
    res=res.substring(0,res.length-1);
    return res;
}
tool.chkCollect=function (userid,subarr) {
    var res=false;
    for(var j=0;j<subarr.length;++j){
        if (subarr[j].MemberId==userid){
            res=true;
            break;
        }
    }
    return res;
}
tool.getCollectcnt=function (driverid,callback) {
    models.Collect.count({where:{DriverId:driverid}}).then(function (cnt) {
        callback(cnt);
    })
}

tool.getNumstr=function (inpnum) {
    if (inpnum>=1000000){
        return parseInt(inpnum/1000000) +"M";
    }
    else if (inpnum>=1000){
        return parseInt(inpnum/1000) +"K";
    }
    else
        return inpnum.toString();
}

tool.isEvaluate=function(userid,courseid,callback){ //是否上過課
    models.Evaluation.count({where:{MemberId:userid,CourseId:courseid}}).then(function (cnt) {
        if (cnt==0) callback("N"); else callback("Y");
    })
}
tool.getDriverevalcnt=function(DriverId,callback){
    models.Evaluation.count({where:{DriverId:DriverId,State:"Y"}}).then(function (cnt) {
        callback(cnt);
    }).catch(function (err) {
        callback(0);
    })
}
tool.averageStar=function(DriverId,star,callback){
    var res=0;
    models.Evaluation.findAll({where:{DriverId:DriverId}}).then(function (docs) {
        if (docs==null||docs.length==0){
            callback(star);
        }
        else {
            var cnt=docs.length;
            var sum=0;
            for (var i=0;i<docs.length;++i){
                sum+=docs[i].Grade;
            }
            res=((sum+star)/(cnt+1)).toFixed(1) ;
            callback(res);
        }
    }).catch(function (err) {
        callback(0);
    })
}

tool.defaultrate=80;

tool.getEvaluation=function (id,callback) {
    models.Evaluation.findOne({where:{id:id}}).then(function (doc) {
        callback(doc);
    })
}

//
tool.chkDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= moment(tmpstr,"YYYY-MM-DD").isValid();
        return res;
    }
    else {
        return false;
    }
}
tool.chkDatearr=function (inparr) {
    for(var i=0;i<inparr.length;++i){
        if (!this.chkDate(inparr[i])){
            return false;
        }
    }
    return true
}

tool.setDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= moment(tmpstr,"YYYY-MM-DD HH:mm");
        return res;
    }
    else {
        return null;
    }
}
tool.strtoDate=function (inpstr) {
    if (inpstr!=null && inpstr!=""){
        var tmpstr=replaceAll(replaceAll(inpstr,"/","-"),".","-");
        var res= new Date(tmpstr);
        return res;
    }
    else {
        return null;
    }
}

function replaceAll(str, find, replace)
{
    while( str.indexOf(find) > -1)
    {
        str = str.replace(find, replace);
    }
    return str;
}
tool.uniqArr=function(arr) {
    var a = [];
    for (var i=0, l=arr.length; i<l; i++)
        if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
            a.push(arr[i]);
    return a;
}
//filter json data usage:var data = filterData(JSON.parse(my_json), {website: 'yahoo'});
tool.filterData=function(my_object, my_criteria){
    return my_object.filter(function(obj) {
        return Object.keys(my_criteria).every(function(c) {
            return obj[c] == my_criteria[c];
        });
    });
}


//sql
tool.query=function (qry,callback) {
    // models.sequelize.query(qry,{ type: models.sequelize.QueryTypes.SELECT}).then(function (res) {
    models.sequelize.query(qry).then(function (res) {
        callback(res);
    })
}
//

module.exports = tool;