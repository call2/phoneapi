/**
 * Created by jack on 1/19/17.
 */
// config
var config = require('config.json')('setting.json');

var winston = require('winston');

// moment: date/time format
var moment = require('moment-timezone');

var logInfo = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: config.LogSuccessPath + moment().tz(config.TimeZone).format(config.DateTimeFormat_YYYYMMDD) + '.log',
            level: 'info',
            timestamp: moment().tz(config.TimeZone).format(config.DateTimeFormat_YYYYMMDDHHmmss)
        })
    ]
});

var logError = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'error-file',
            filename: config.LogErrorPath + moment().tz(config.TimeZone).format(config.DateTimeFormat_YYYYMMDD) + '.log',
            level: 'error',
            timestamp: moment().tz(config.TimeZone).format(config.DateTimeFormat_YYYYMMDDHHmmss)
        })
    ]
});

function BoxLogger() {

}

BoxLogger.Info = function (type, msg) {
    logInfo.info(type, msg);
};

BoxLogger.Error = function (type, msg) {
    logError.error(type, msg);
};

module.exports = BoxLogger;