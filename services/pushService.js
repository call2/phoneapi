
var FCM = require('fcm-push');  //Android
var apn = require('apn');   //iOS
var config = require('config.json')('setting.json');
var models  = require('../models');
var Sequelize = require("sequelize");

function PushService() {

}
PushService.sendPushByOne = function(MemberID, Content,Type,Typeid, callback) {
    //傳送推播給司機
    //1.乘客呼叫代駕 :傳送推播給司機 (若接受call api:0402.acceptCase)
    //2.取消呼叫(已配對成功後) :傳送推播給司機
    //3.餘額不足推播銀行帳號給司機 提醒儲值(司機銀行帳號會放在Content)
    //4.司機接受案件:傳送推播給乘客 (a.停掉timer b.call 0205.getDriverinfo)
    //5.司機按下抵達:傳送推播給乘客 (顯示抵達UI)
    //6.司機按下開始:傳送推播給乘客 (顯示開始時間 +隱藏[取消呼叫]按鈕)
    //7.司機按下結束:傳送推播給乘客 (顯示到達目的地訊息框)
    getPushtoken(Type,MemberID,function (user) {
        if (user!=null){
            var notification = {
                "Typeid": Typeid,
                "Type": Type,
                "Content": Content
            }
            if ("123".indexOf(Type)<0)
                notification["MemberId"]=MemberID;
            else
                notification["DriverId"]=MemberID;
            models.Pushlog.create(notification).then(function () {
                if(user.Devicetype == "A"){
                    //Android
                    PushService.AndroidPush(user.Pushtoken, Content,Type,Typeid);
                    callback(true);
                } else  if(user.Devicetype == "I"){
                    //iOS
                    PushService.iOSPush(user.Pushtoken, Content,Type,Typeid);
                    callback(true);
                }
            })
        }
        else
            callback(false);
    })
};
function getPushtoken(type,id,callback) {
    if ("123".indexOf(type)<0){
        models.Member.findOne({where:{id: id}}).then(function (user) {
            callback(user)
        })
    }
    else {
        models.Driver.findOne({where:{id: id}}).then(function (user) {
            callback(user)
        })
    }
}
PushService.AndroidPush = function (UUID, Alert,Type,Typeid) {
    var apiKey = config.Push.AndroidAPIKey;
    var fcm = new FCM(apiKey);
    var tokens = UUID;
    var message = {
        to: tokens, // required
        notification: {
            //title: 'Title of your push notification',
            body: Alert,
            Type:Type,
            Typeid:Typeid
        }
    };

    //callback style
    fcm.send(message, function(err, response){
        if (err) {
            console.log(err);
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: ", response);
        }
    });
};


PushService.iOSPush = function (UUID, Alert,Type,Typeid) {

    var pem = "./services/DriverPushDeveloper.pem";
    var key = "./services/DriverPushDeveloper.pem";
    // var pem = "./services/DriverPushStore.pem";
    // var key = "./services/DriverPushStore.pem";

    var tokens = UUID;
    var service = new apn.Provider({
        "cert": pem,
        "key": key,
        "passphrase":"55555555",
        "production": false
    });

    var note = new apn.Notification({
        "alert": Alert,
        "sound":"default",
        "Type":Type,
        "Typeid":Typeid
        //"sound":"cat2.wav"
    });

    // note.badge = notReadCount;

    service.send(note, tokens);

    service.shutdown();
};

module.exports = PushService;