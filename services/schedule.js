/**
 * Created by ralph on 8/31/2017.
 */

// var express = require('express');
// var router = express.Router();

//PushNotification
// var pushNotification = require('../lib/userSendPush');
//Errors
var errors = require('../lib/errors');
//box logger
var boxlogger = require('../lib/boxloger');
//db
var models  = require('../models');
// config
var config = require('config.json')('setting.json');
// winston logging
var winston = require('winston');
// moment: date/time format
var moment = require('moment-timezone');
// util: like a C++ printf format
var util = require('util');
var tool=require('../lib/tool')
//
var schedulejob = require('node-schedule');


function schedule() {

}

schedule.Start=function(){
    schedulejob.scheduleJob('0 10 1 1 * *', function () {
        console.log("*****1. 每月刪除Phonerecord");
        var ymd=moment().tz("Asia/Taipe").add(-1,"Months").format("YYYY-MM")+"-01";
        var chkdate=moment(ymd,"YYYY-MM-DD");
        var enddate=moment(ymd,"YYYY-MM-DD").add(1,"Months");
        models.Phonerecord.delete({where:{createdAt:{$lt:chkdate}}}).then(function () {

        })
        //
        console.log("*****2. 更新member.sum1");
        tool.query("UPDATE phoneDB.Members set sum1=0").then(function () {
            var sql="UPDATE phoneDB.Members AS t1  SET t1.Sum1 = " +
                "(SELECT cnt FROM " +
                "(select phone, count(*) cnt from phoneDB.Salesrecords where isbuy='Y' and Cmsdate>='"+ymd+"' and Cmsdate<'"+moment(enddate).format("YYYY-MM-DD")+"' group by phone) t2 " +
                "WHERE t2.Phone = t1.Phone)";
            tool.query(sql).then(function () {

            })
        })
        // models.Member.findAll({where:{Deleteflag:"N"},include:[{model:models.Salesrecord,where:{$and:[{Cmsdate:{$ge:chkdate}},{Cmsdate:{$lt:enddate}}]}}]}).then(function (docs) {
        //     for (var i=0;i<docs.length;++i){
        //         docs[i].Sum1=docs[i].Salesrecords.length;
        //         docs[i].save();
        //     }
        // })
    })
}

module.exports = schedule;