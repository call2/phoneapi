var crypto = require('crypto');

var sendSMS = function(to, message) {
    var promise = new Parse.Promise();
    Parse.Cloud.httpRequest({
        method: 'GET',
        url: 'http://smexpress.mitake.com.tw/SmSendGet.asp?username=' +''+ '&password=' +''+ '&dstaddr=' + to + '&smbody=' + message + '&encoding=UTF8',
        header: 'content-type: application/json',
        success: function(httpResponse) {
            promise.resolve(httpResponse);
        },
        error: function(error) {
            promise.error(error);
        }
    });
    return promise;
};

var confirmationMobile = function(id, code) {

};

var md5 = function(str) {
    var keys = {1:0, 3:8, 5:6, 7:4, 9:2};
    return crypto
        .createHash('md5')
        .update(str)
        .digest('hex')
        .replace(/[13579]/g, function(item) {
            return keys[item];
        });
};

Parse.Cloud.beforeSave('Mobile', function(request, response) {
    var mobileNumber = request.object.get('mobileNumber');

    if (!/^09[0-9]{8}$/.test(mobileNumber)) {
        response.error('行動電話號碼錯誤');
    } else if (!request.object.existed()) {
        var qUser = new Parse.Query(Parse.User);
        qUser.equalTo('username', mobileNumber);
        qUser.first({useMasterKey: true}).then(function(user) {
            if (user) {
                var confirmationCode = (Math.floor(Math.random() * (999999 - 111111 + 1)) + 111111).toString();
                request.object.set('confirmationCode', md5(confirmationCode));

                request.object.set('expiredAt', new Date(Date.now() + 2 * 60 * 1000));

                sendSMS(mobileNumber, '硬現場APP簡訊驗證碼: ' + confirmationCode + ' ，如非本人操作則無須理會');
                response.success();
            } else {
                response.error('行動電話尚未註冊');
            }
        }, function() {
            response.error('行動電話尚未註冊');
        });
    } else if(request.master) {
        response.success();
    } else {
        response.error('cannot update mobile');
    }
});

/**
 * @param  password
 * @param  confirmationId
 * @param  confirmationCode
 * @return bool
 */
Parse.Cloud.define('forgotPassword', function(request, response) {
    var confirmationId = request.params.confirmationId;
    var confirmationCode = md5(request.params.confirmationCode || '');
    var password = request.params.password;

    var qMobile = new Parse.Query('Mobile');
    qMobile.get(confirmationId, {useMasterKey: true}).then(function(mobile) {
        if (mobile.get('expiredAt').getTime() < Date.now() || mobile.get('usedAt')) {
            response.error('驗證碼已過期');
        } else if (confirmationCode !== mobile.get('confirmationCode')) {
            response.error('驗證碼錯誤');
        } else {
            mobile.save({'usedAt': new Date()}, {useMasterKey: true});

            var qUser = new Parse.Query(Parse.User);
            qUser.equalTo('username', mobile.get('mobileNumber'));
            qUser.first().then(function(user) {
                user.save({'password': password}, {useMasterKey: true}).then(function() {
                    response.success();
                }, function(error) {
                    response.error('更新密碼失敗');
                });
            });
        }
    }, function() {
        response.error('驗證碼錯誤');
    });
});

/**
 * @param  verificationId
 * @param  verificationCode
 * @return bool
 */
Parse.Cloud.define('verifyMobile', function(request, response) {
    var verificationId = request.params.verificationId;
    var verificationCode = md5(request.params.verificationCode || '');

    var qMobile = new Parse.Query('Mobile');
    qMobile.get(verificationId, {useMasterKey: true}).then(function(mobile) {
        if (mobile.get('expiredAt').getTime() < Date.now() || mobile.get('usedAt')) {
            response.error('驗證碼已過期');
        } else if (verificationCode !== mobile.get('confirmationCode')) {
            response.error('驗證碼錯誤');
        } else {
            mobile.save({'usedAt': new Date()}, {useMasterKey: true});

            var qUser = new Parse.Query(Parse.User);
            qUser.equalTo('username', mobile.get('mobileNumber'));
            qUser.first().then(function(user) {
                user.save({'mobileVerified': true}, {useMasterKey: true}).then(function() {
                    response.success();
                }, function(error) {
                    response.error('驗證行動電話失敗');
                });
            });
        }
    }, function() {
        response.error('驗證碼錯誤');
    });
});